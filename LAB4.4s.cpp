// LAB4.4s.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>
#include <ctime>

using namespace std;

vector <int> delete_vec;
vector <int> keys;

struct couple {
	int number;
	int key;
};

vector <couple> sort;

template <class T>
class P_Queue {
	class Element {
		int key;
		T data;
		friend class P_Queue;
		Element *next;
		Element *previous;
		Element() {
			key = 0;
			data = NULL;
			next = NULL;
			previous = NULL;
		}
	};
public:
	Element *tail;
	Element *head;
	P_Queue() {
		tail = NULL;
		head = NULL;
	}
	void Add_Element(T data_t, int key_t) {
		Element *temp = new Element;
		temp->data = data_t;
		temp->key = key_t;
		temp->next = NULL;
		if (head == NULL)
			head = temp;
		else {
			tail->next = temp;
		}
		tail = temp;
	}

	int Size() {
		Element *actual = new Element;
		int size = 0;
		actual = head;
		while (actual != NULL) {
			actual = actual->next;
			size += 1;
		}
		return size;
	}
	void Delete_First() {
		if (head != NULL) {
			Element *temp = new Element;
			temp = head;
			head = head->next;
			delete temp;
		}
		else
			cout << "Kolejka jest pusta!" << endl;
	}
	void Delete_Last() {
		if (tail != NULL) {
			Element *actual = new Element;
			Element *previous = new Element;
			actual = head;
			while (actual->next != NULL) {
				previous = actual;
				actual = actual->next;
			}
			tail = previous;
			previous->next = NULL;
			delete actual;
		}
		else
			cout << "Kolejka jest pusta!" << endl;
	}
	void Delete_Any(int any) {
		Element *actual = new Element;
		Element *previous = new Element;
		actual = head;
		for (int i = 1; i < any; i++) {
			if (actual == NULL) {
				cout << "Element z poza listy!" << endl;
				break;
			}
			else {
				previous = actual;
				actual = actual->next;
			}
		}
		if (actual != NULL) {
			previous->next = actual->next;
		}
	}
	int Show_Min() {
		Element *actual = new Element;
		actual = head;
		int min_key;
		if (head == NULL) {
			cout << "Kolejka jest pusta!" << endl;
		}
		else {
			min_key = actual->key;
			while (actual != NULL) {
				if (actual->key < min_key) {
					min_key = actual->key;
				}
				actual = actual->next;
			}
			//cout << "Najmniejszy klucz to: " << min_key << endl;
			actual = head;
			int position = 1;
			while (actual != NULL) {
				//if (actual->key == min_key)
					//cout << "Element: " << actual->data << " jest o najmniejszym kluczu rownym " << actual->key << " z pozycji nr. " << position << endl;
				actual = actual->next;
				position += 1;
			}
		}
		return (min_key);
	}
	couple Delete_Min() {
		Element *temp = new Element;
		Element *actual = new Element;
		Element *previous = new Element;
		couple chosen;
		actual = head;
		int min_key;
		int position = 1;
		int correction = 0;
		if (head == NULL) {
			cout << "Kolejka jest pusta!" << endl;
		}
		else {
			min_key = Show_Min();
			while (actual != NULL) {
				if (actual->key == min_key) {
					//cout << "Usuwam element: " << actual->data << " o najmniejszym kluczu " << actual->key << " z pozycji nr. " << position << endl;
					chosen.key = actual->key;
					chosen.number = actual->data;
					delete_vec.push_back(position);
				}
				previous = actual;
				actual = actual->next;
				position += 1;
			}
		}
		for (int i = 0; i < delete_vec.size(); i++) {
			if (Size() > 1) {
				if ((delete_vec[i] - correction) == 1 || Size() == 1) { //   USUWANIE ELEMENTU Z POCZATKU
					Delete_First();
					correction += 1;
				}
				if ((delete_vec[i] - correction) == Size() && (delete_vec[i] - correction) != 1) { //    USUWANIE ELEMENTU Z KONCA
					Delete_Last();
					correction += 1;
				}
				if ((delete_vec[i] - correction) != 1 && (delete_vec[i] - correction) != Size()) {//    USUWANIE ELEMENTU ZE SRODKA KOLEJKI
					Delete_Any(delete_vec[i] - correction);
					correction += 1;
				}
			}
			else {
				Delete_First();
				correction += 1;
				break;
			}
		}
		delete_vec.clear();
		return chosen;
	}
	void Show_All() {
		Element *help = new Element;
		help = head;
		if (head == NULL) {
			cout << "Kolejka jest pusta!" << endl;
		}
		else {
			cout << "Poczatek kolejki" << endl;
			while (help != NULL) {
				cout << " Element: " << help->data << "\t\t Klucz: " << help->key << endl;
				help = help->next;
			}
			cout << "Koniec kolejki" << endl;
		}
	}
	void Delete_All() {
		Element *temp = new Element;
		if (head = NULL) {
			cout << "Kolejka jest pusta" << endl;
		}
		else {
			while (head != NULL) {
				temp = head;
				head = head->next;
				delete temp;
			}
		}
		head = tail = NULL;
	}

};

bool Test_key(int test) {
	for (int i = 0; i < keys.size(); i++) {
		if (keys[i] == test)
			return true;
	}
	return false;
}

int main()
{
	P_Queue <int> queue;
	int data_t;
	int key_t;
	int how_much;
	int test;
	srand((int)time(NULL));
	cout << "### KOLEJKA PRIORYTETOWA - SORTOWANIE PRZEZ WYBIERANIE ###" << endl;
	Sleep(1500);
	cout << endl << "Podaj ilosc elementow ktore posortujemy: ";
	sort.clear();
	cin >> how_much;
	for (int i = 0; i < how_much; i++) {
		data_t = rand();
		test = rand();
		while (Test_key(test)==1) {
			test = rand();
		}
		keys.push_back(test);
		key_t = test;
		queue.Add_Element(data_t, key_t);
	}
	Sleep(1000);
	cout << endl << "Kolejka przed sortowaniem" << endl << endl;
	Sleep(1500);
	queue.Show_All();
	Sleep(10000);
	cout << endl << "Kolejka po sortowaniu" << endl << endl;
	Sleep(1500);
	for (int j = 0; j < how_much; j++) {
		sort.push_back(queue.Delete_Min());
	}
	cout << "Poczatek kolejki" << endl;
	for (int i = 0; i < how_much; i++) {
		cout << "Element: "<<sort[i].number << "\t\t Klucz: ";
		cout << sort[i].key << endl;
	}
	cout << "Koniec kolejki" << endl;
	Sleep(10000);
	cout <<endl<<"### KONIEC PREZENTACJI DZIALANIA PROGRAMU ###" << endl;
	queue.Delete_All();
	keys.clear();
	sort.clear();
	system("PAUSE");
	return 0;
}

