// LAB4.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>

using namespace std;

vector <int> delete_vec;
template <class T>
class P_Queue {
	class Element {
		int key;
		T data;
		Element *next;
		Element *previous;
		friend class P_Queue;
	};
	Element *tail;
	Element *head;
public:
	P_Queue() {
		tail = NULL;
		head = NULL;
	}
	void Add_Element() {
		T data_t;
		int key_t;
		Element *temp = new Element;
		cout << "Podaj element jaki chcesz dodac: ";
		cin >> data_t;
		cout << "Podaj klucz (priorytet) elementu: ";
		cin >> key_t;
		temp->data = data_t;
		temp->key = key_t;
		temp->next = NULL;
		if (head == NULL)
			head = temp;
		else
			tail->next = temp;
		tail = temp;
	}
	void Add_Element_AUTO(T data_t, int key_t) {
		Element *temp = new Element;
		temp->data = data_t;
		temp->key = key_t;
		temp->next = NULL;
		if (head == NULL)
			head = temp;
		else
			tail->next = temp;
		tail = temp;
	}
	int Size() {
		Element *actual = new Element;
		int size = 0;
		actual = head;
		while (actual != NULL) {
			actual = actual->next;
			size += 1;
		}
		return size;
	}
	void Delete_First() {
		if (head != NULL) {
			Element *temp = new Element;
			temp = head;
			head = head->next;
			delete temp;
		}
		else
			cout << "Kolejka jest pusta!" << endl;
	}
	void Delete_Last() {
		if (tail != NULL) {
			Element *actual = new Element;
			Element *previous = new Element;
			actual = head;
			while (actual->next != NULL) {
				previous = actual;
				actual = actual->next;
			}
			tail = previous;
			previous->next = NULL;
			delete actual;
		}
		else
			cout << "Kolejka jest pusta!" << endl;
	}
	void Delete_Any(int any) {
		Element *actual = new Element;
		Element *previous = new Element;
		actual = head;
		for (int i = 1; i < any; i++) {
			if (actual == NULL) {
				cout << "Element z poza listy!" << endl;
				break;
			}
			else {
				previous = actual;
				actual = actual->next;
			}
		}
		if (actual != NULL) {
			previous->next = actual->next;
		}
	}
	int Show_Min() {
		Element *actual = new Element;
		actual = head;
		int min_key;
		if (head == NULL) {
			cout << "Kolejka jest pusta!" << endl;
		}
		else {
			min_key = actual->key;
			while (actual != NULL) {
				if (actual->key < min_key) {
					min_key = actual->key;
				}
				actual = actual->next;
			}
			cout << "Najmniejszy klucz to: " << min_key << endl;
			actual = head;
			int position = 1;
			while (actual != NULL) {
				if (actual->key == min_key)
					cout << "Element: " << actual->data << " jest o najmniejszym kluczu rownym " << actual->key << " z pozycji nr. " << position << endl;
				actual = actual->next;
				position += 1;
			}
		}
		return (min_key);
	}
	void Delete_Min() {
		Element *temp = new Element;
		Element *actual = new Element;
		Element *previous = new Element;
		actual = head;
		int min_key;
		int position = 1;
		int correction = 0;
		if (head == NULL) {
			cout << "Kolejka jest pusta!" << endl;
		}
		else {
			min_key = Show_Min();
			while (actual != NULL) {
				if (actual->key == min_key) {
					cout << "Usuwam element: " << actual->data << " o najmniejszym kluczu " << actual->key << " z pozycji nr. " << position << endl;
					delete_vec.push_back(position);
				}
				previous = actual;
				actual = actual->next;
				position += 1;
			}
		}
		for (int i = 0; i < delete_vec.size(); i++) {
			if (Size() > 1) {
				if ((delete_vec[i] - correction) == 1 || Size() == 1) { //   USUWANIE ELEMENTU Z POCZATKU
					Delete_First();
					correction += 1;
				}
				if ((delete_vec[i] - correction) == Size() && (delete_vec[i] - correction) != 1) { //    USUWANIE ELEMENTU Z KONCA
					Delete_Last();
					correction += 1;
				}
				if ((delete_vec[i] - correction) != 1 && (delete_vec[i] - correction) != Size()) {//    USUWANIE ELEMENTU ZE SRODKA KOLEJKI
					Delete_Any(delete_vec[i] - correction);
					correction += 1;
				}
			}
			else {
				Delete_First();
				correction += 1;
				break;
			}
		}
		delete_vec.clear();
	}
	void Show_All() {
		Element *help = new Element;
		help = head;
		if (head == NULL) {
			cout << "Kolejka jest pusta!" << endl;
		}
		else {
			cout << "Poczatek kolejki" << endl;
			while (help != NULL) {
				cout << " Element: " << help->data << "\t\t Klucz: " << help->key << endl;
				help = help->next;
			}
			cout << "Koniec kolejki" << endl;
		}
	}
	void Delete_All() {
		Element *temp = new Element;
		if (head = NULL) {
			cout << "Kolejka jest pusta" << endl;
		}
		else {
			while (head != NULL) {
				temp = head;
				head = head->next;
				delete temp;
			}
		}
		head = tail = NULL;
	}
};

void show_menu() {
	system("cls");
	cout << endl << endl << "### MENU OBSLUGI KOLEJKI PRIORYTETOWEJ ###" << endl;
	cout << "1. Dodaj element do kolejki" << endl;
	cout << "2. Usun element z kolejki (z najmniejszym kluczem)" << endl;
	cout << "3. Wyswietl elementy kolejki" << endl;
	cout << "4. Pokaz element z najmniejszym kluczem" << endl;
	cout << "5. Usun wszystkie elementy" << endl;
	cout << "6. Wyswietl elementy kolejki" << endl;
	cout << "0. Koniec" << endl;
	cout << "Podaj swoj wybor : ";
}
void presentation(P_Queue <int> &queue) {
	system("cls");
	queue.Delete_All();
	cout << endl << "Prezentacja dzialania programu" << endl << endl;
	Sleep(1000);
	cout << endl << "Dodamy 5 elementow do kolejki" << endl << endl;
	Sleep(1000);
	//cout << endl << "Elementy musza miec rozne priorytety!!" << endl << endl;
	//Sleep(2000);
	cout << endl << "Pierwszy element o wartosci 5 i kluczu 4" << endl << endl;
	Sleep(500);
	queue.Add_Element_AUTO(5, 4);
	Sleep(1000);
	cout << endl << "Drugi element o wartosci 7 i kluczu 2" << endl << endl;
	Sleep(500);
	queue.Add_Element_AUTO(7, 2);
	Sleep(1000);
	cout << endl << "Trzeci element o wartosci 4 i kluczu 5" << endl << endl;
	Sleep(500);
	queue.Add_Element_AUTO(4, 5);
	Sleep(1000);
	cout << endl << "Czwarty element o wartosci 4 i kluczu 1" << endl << endl;
	Sleep(500);
	queue.Add_Element_AUTO(4, 1);
	Sleep(1000);
	cout << endl << "Piaty element o wartosci 7 i kluczu 3" << endl << endl;
	Sleep(500);
	queue.Add_Element_AUTO(7, 3);
	Sleep(4000);
	cout << endl << "Wyswietlimy zawartosc kolejki" << endl << endl;
	Sleep(1000);
	queue.Show_All();
	Sleep(7000);
	cout << endl << "Wyswietlimy rozmiar kolejki" << endl << endl;
	Sleep(1000);
	cout << "Rozmiar kolejki to: " << queue.Size() << endl;
	Sleep(4000);
	cout << endl << "Pokazemy element o najmniejszym kluczu (priorytecie)" << endl << endl;
	Sleep(1000);
	queue.Show_Min();
	Sleep(7000);
	cout << endl << "Usuniemy element o najmniejszym kluczu (priorytecie)" << endl << endl;
	Sleep(1000);
	queue.Delete_Min();
	Sleep(7000);
	cout << endl << "Wyswietlimy zawartosc kolejki" << endl << endl;
	Sleep(1000);
	queue.Show_All();
	Sleep(7000);
	cout << endl << "Usuniemy cala zawartosc kolejki" << endl << endl;
	Sleep(1000);
	queue.Delete_All();
	Sleep(2000);
	cout << endl <<"Sprawdzimy czy jest pusta" << endl << endl;
	Sleep(1000);
	queue.Show_All();
	Sleep(4000);
	cout << endl << "### Koniec Prezentacji dzialania kolejki, powrot do MENU ###" << endl << endl;
	Sleep(4000);

}
int main()
{
	P_Queue <int> queue;
	int opt, opt2;
	do {
		system("cls");
		cout << endl << endl << "### WYBOR PREZENTACJA BADZ MENU KOLEJKI ###" << endl;
		cout << "1. Prezentacja dzialania programu (brak menu)" << endl;
		cout << "2. Menu obslugi kolejki priorytetowej" << endl;
		cout << "0. Wyjscie z programu" << endl;
		cout << "Podaj swoj wybor: ";
		cin >> opt;
		switch (opt) {
		case 1: presentation(queue); break;
		case 2: 
			do {
				show_menu();
				cin >> opt2;
				switch (opt2) {
				case 1: queue.Add_Element();  Sleep(1000); break;
				case 2: queue.Delete_Min();  Sleep(3000);  break;
				case 3: queue.Show_All();  Sleep(7000); break;
				case 4: queue.Show_Min(); Sleep(5000); break;
				case 5: queue.Delete_All(); cout << "Usunieto wszystkie elementy kolejki" << endl; Sleep(3000); break;
				case 6: cout << "Rozmiar kolejki: " << queue.Size() << endl; Sleep(3000); break;
				case 0: cout << "Powrot do glownego menu" << endl; break;
				default: cout << "Nie ma takiej opcji w menu!!" << endl;
				}
			} while (opt2 != 0);
			break;
		case 0: cout << "Wychodzisz z programu!" << endl;  break;
		}
	} while (opt != 0);
	queue.Delete_All();
	system("PAUSE");
	return 0;
}

