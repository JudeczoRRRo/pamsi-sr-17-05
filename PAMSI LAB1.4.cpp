// ConsoleApplication7.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

bool jestPal(const string& testStr,int poczatek, int koniec) {
    if(testStr[poczatek] == testStr[koniec]){
        if(koniec-poczatek <1){
            return true;
        }
        else{
            jestPal(testStr, poczatek+1, koniec-1);
        }
    }
    else return false;
}
int main()
{
	string testStr;
	cout << "Podaj slowo, sprawdzimy czy jest palindromem: ";
	cin >> testStr;
	if (jestPal(testStr,0,testStr.length()-1) == 1) {
		cout << "Slowo " << testStr << " jest palindromem" << endl;
	}
	else {
		cout << "Slowo " << testStr << " nie jest palindromem" << endl;
	}
	//system("pause");
    return 0;
}

