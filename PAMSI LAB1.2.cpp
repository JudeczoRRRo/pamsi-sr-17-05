// ConsoleApplication8.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
using namespace std;
void menu() {
	cout << "****MENU****" << endl<< "1. Wypelnienie tablicy wartosciami losowymi" << endl << "2. Zapis tablicy jednowymiarowej do pliku tekstowego" << endl << "3. Wczytanie tablicy jednowymairowej z pliku tekstowego" << endl;
	cout << "4. Zapis tablicy jednowymiarowej do pliku binarnego" << endl << "5. Wczytanie tablicy jednowymairowej z pliku binarnego" << endl << "6. Wyswietlenie tablicy" << endl;
	cout << "0. Wyjscie z programu" << endl << "Podaj numer opcji menu, ktora Cie interesuje: ";
}
int wypelnij(double tab[], int r) {
	for (int i = 0; i < r; i++) {
		tab[i] = rand() % 100 + 1;
	}
	return tab[r];
}
void zapiszT(string nazwa_pliku, double tab[],int r) {

	std::fstream plik;
	plik.open(nazwa_pliku, std::ios::in | std::ios::out);
	if( !pliktxt.good() )
	{
		cout << "Plik nie zostal utworzony!\n\n";
	}
	else
	{
		for (int i = 0; i < r; i++) {
			plik << tab[i]<< "\n";
		}
		cout << "Dane zostaly zapisane!\n\n";
		plik.close();
	}

}
int wczytajT(string nazwa_pliku) {
	std::fstream plik;
	int tabW[5000];
	int i = 0, licznik;
	plik.open(nazwa_pliku);
	while (!plik.eof())
	{
		plik >> tabW[i];

		i++;
	}
	licznik = i-1;
	plik.close();
	for (i = 0; i < licznik; i++) {
		cout << tabW[i] << " ";
	}
	return tabW[5000];
}
void zapiszB(double tab[], int r, string nazwa_pliku) {
	ofstream plik(nazwa_pliku, ios::binary);
	//plik.write((char*)&r, sizeof(int));
	{
		for (int i = 0; i < r; i++) {
			//double &ref = *tab[i];
			plik.write((char*)&tab, sizeof(tab));
		}
		plik.close();
	}
}

void wczytajB(string nazwa_pliku, double tab[], int r) {
	ifstream plik(nazwa_pliku, ios::binary);
	//plik.read((char*)&r, sizeof(int));
	for (int i = 0; i < r; i++) {
		plik.read((char*)tab, sizeof(tab));
	}
	plik.close();
}
void wyswietl(double tab[], int r) {
	for (int i = 0; i < r; i++) {
		cout << tab[i] << " ";
		if (i % 10 == 0 && i!=0) {
			cout << endl;
		}
	}
	cout << endl;
}

int main()
{
	string nazwa_pliku;
	int r,opcja = 1;

	cout << "Podaj rozmiar tablicy: ";
	cin >> r;
	fstream plik;
	double* tab = new double[r];
	for (int i = 0; i < r; ++i)
		tab[i] = 0;
	do{
		menu();
		cin >> opcja;
		switch (opcja) {
		case 0: break;
		case 1: { wypelnij(tab, r);  break; }
		case 2: {
			cout << "Podaj nazwe pliku na ktorym chcesz operowac (*.txt): ";
			cin >> nazwa_pliku;
			zapiszT(nazwa_pliku, tab, r); break; }
		case 3: {
			cout << "Podaj nazwe pliku na ktorym chcesz operowac(*.txt): ";
			cin >> nazwa_pliku;
			wczytajT(nazwa_pliku); break; }
		case 4: {
			cout << "Podaj nazwe pliku na ktorym chcesz operowac(*.bin): ";
			cin >> nazwa_pliku;
			zapiszB(tab, r, nazwa_pliku); break; }
		case 5: {
			cout << "Podaj nazwe pliku na ktorym chcesz operowac(*.bin): ";
			cin >> nazwa_pliku;
			wczytajB(nazwa_pliku,tab,r);  break; }
		case 6: { wyswietl(tab, r); break; }

		};
	}while(opcja!=0);

	delete[] tab;
	tab = NULL;
	//system("pause");
    return 0;
}

