// LAB7.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cstdlib>
#include <ctime>
#include <list>
#include <vector>
#include "iostream"

using namespace std;
int  probkowanie = 0;
int losowanie;

int losuj() {
	return (rand()%losowanie)+5;
}

bool Czy_Byla(int Liczba, int tab_los[], int ile)
{
	if (ile <= 0)
		return false;
	int i = 0;
	do{
		if (tab_los[i] == Liczba)
			return true;
		i++;
	} while (i < ile);
	return false;
}

void Wylosuj_wszystko(int *tab_los, int ilosc) {
	int wylosowane = 0;
	int x;
	while (wylosowane<ilosc) {
		x = losuj();
		if (Czy_Byla(x, tab_los, wylosowane) == false) {
			tab_los[wylosowane] = x;
			wylosowane++;
		}
	}
}

void Probkowanie_Liniowe(int *tab, int x, int size) {
	if (((x % size) + 1) == size) {
		for (int i = 0; i < size; i++) {
			if (tab[i] == 0) {
				tab[i] = x;
				probkowanie++;
				break;
			}
		}
	}
	else {
		for (int i = ((x % size) + 1); i != (x % size); i++) {
			if (tab[i] == 0) {
				tab[i] = x;
				probkowanie++;
				break;
			}
			if (i == size - 1)
				i = -1;
		}
	}
}

void Dodaj_Elementy(int *tab, int ilosc, int tab_los[],int size) {
	for (int i = 0; i < ilosc;i++) {
		if (tab[tab_los[i] % size] == 0) {
			tab[tab_los[i] % size] = tab_los[i];
			probkowanie++;
		}
		else
			Probkowanie_Liniowe(tab, tab_los[i], size);
	}
}

bool Czy_Pierwsza(int n)
{
	if (n<2)
		return false;
	for (int i = 2; i*i <= n; i++)
		if (n%i == 0)
			return false;
	return true;
}

void Dodaj_Element_Double_Hash(int *tab, int ilosc, int tab_los[], int size) {
	int N = size;
	int q = N - 1;
	for (int i = 0; i < N-1; i++) {
		if (Czy_Pierwsza(q) == false) {
			q--;
		}
		else
			break;
	}
	for (int i = 0; i < ilosc; i++) {
		if (tab[tab_los[i] % N % q] == 0) {
			tab[tab_los[i] % N % q] = tab_los[i];
		}
		else
			Probkowanie_Liniowe(tab, tab_los[i], size);
	}
}

int Szukaj_element(int tab[], int size, int x, int &miejsce) {
	int element=0;
	if ((x%size) != 0) {
		for (int i = (x % size); i != (x % size) - 1; i++) {
			if (tab[i] == x) {
				element = x;
				miejsce = i;
				break;
			}
			if (i == (size - 1)) {
				i = -1;
			}
		}
	}
	else
		for (int i = 0; i < size; i++) {
			if (tab[i] == x) {
				element = x;
				miejsce = i;
				break;
			}
		}
	return element;
}

void Dodaj_Element_link(vector <int> *tab_link, int ilosc, int tab_los[], int size) {
	for (int i = 0; i < ilosc; i++) {
		tab_link[tab_los[i] % size].push_back(tab_los[i]);
	}
}

void Wyswietl_Tablice_link(vector <int> tab_link[], int size) {
	for (int i = 0; i < size; i++) {
		cout << i << ":\t";
		for (int j = 0; j < tab_link[i].size(); j++) {
			cout << tab_link[i][j] << "\t";
		}
		cout << endl;
	}
}

int Szukaj_element_link(vector <int> tab_link[], int size, int x) {
	int element = 0;
	for (int i = 0; i < tab_link[x%size].size(); i++) {
		if (tab_link[x%size][i] == x) {
			element = x;
			break;
		}
	}
	return element;
}

void Usun_element_link(vector <int> tab_link[], int size, int x) {

	for (int i = 0; i < tab_link[x%size].size(); i++) {
		if (tab_link[x%size][i] == x) {
			tab_link[x%size].erase(tab_link[x%size].begin()+i);
		}
	}
}
int main()
{	
	srand(time(0));
	int size, size2, ilosc, opt=0, opt1 = 0, opt2 = 0, opt3 = 0, zajete = 0;
	do {
		cout << "#### Podwojne haszowanie tylko dla tablicy o rozmiarze powyzej 3 ####" << endl;
		cout << "Podaj rozmiar tablicy: ";
		cin >> size;
		if (Czy_Pierwsza(size) == false) {
			cout << "Ten rozmiar nie jest liczba pierwsza, podaj nowy" << endl;
		}
	} while (Czy_Pierwsza(size) != true);
	int *tab = new int[size];
	size2 = size * 10;
	losowanie = size * 4;
	int *tab_los = new int[size2];
	vector <int> *tab_link = new vector<int>[size];
	for (int i = 0; i < size; i++) {
		tab[i] = 0;
	}
	do {
		system("cls");
		cout << endl << "Wybierz sposob obslugi tablicy haszujacej o rozmiarze " << size <<endl << endl;
		cout << "1. Probkowanie Liniowe" << endl;
		cout << "2. Podwojne Haszowanie (tylko dla tablicy o rozmairze wiekszym od 3)" << endl;
		cout << "3. Linkowanie" << endl;
		cout << "4. Koniec pracy programu" << endl;
		cout << "Podaj numer opcji, ktora cie interesuje: ";
		cin >> opt;
		system("cls");
		switch (opt) {
		case 1: {
			cout << endl << "### Probkowanie Liniowe ###" << endl;
			do {
				cout << endl;
				cout << endl << "1. Dodaj losowe elementy do tablicy #" << "                                      " << "Calkowita ilosc probek: " << probkowanie << endl;
				cout << "2. Wyswietl tablice haszujaca" << endl;
				cout << "3. Wyszukaj element" << endl;
				cout << "4. Usun element" << endl;
				cout << "5. Powrot do menu glownego" << endl;
				cout << "Podaj numer opcji, ktora cie interesuje: ";
				cin >> opt1;
				switch (opt1) {
					case 1: {
					cout << "Podaj ile elementow chcesz dodac: ";
					cin >> ilosc;
					if (size < (zajete + ilosc)) {
						cout << endl << "Nie udalo sie dodac elementow do tablicy haszujacej" << endl;
						cout << "Taka ilosc elementow, przekroczy zakres rozmiaru tablicy, ktory podales na poczatku" << endl;
						cout << "Maksymalna ilosc elementow jaka mozesz teraz dodac to " << (size - zajete) << endl;
					}
					else {
						Wylosuj_wszystko(tab_los, ilosc);
						Dodaj_Elementy(tab, ilosc, tab_los, size);
						zajete = ilosc + zajete;
						cout << endl << "Pomyslnie dodano elementy do tablicy haszujacej" << endl;
					}
					break;
				}
					case 2: {
						cout << endl << "Tablica haszujaca" << endl;
					for (int i = 0; i < size; i++) {
						cout << i << ":\t";
						if (tab[i] != 0)
							cout << tab[i] << " ";
						else
							cout << "# ";
						cout << endl;
					}
					break;
				}
					case 3: {
					int x;
					int miejsce = 0;
					cout << "Podaj element jaki mamy znalezc: ";
					cin >> x;
					if (Szukaj_element(tab, size, x, miejsce) != 0)
						cout << endl << "W tablicy istnieje element " << Szukaj_element(tab, size, x, miejsce) << " na " << miejsce << " miejscu w tablicy haszujacej" << endl;
					else
						cout << endl << "W tablicy nie ma takiego elementu" << endl;
					break;
				}
					case 4: {
					int x_del;
					int miejsce_del = 0;
					cout << "Podaj element jaki chcesz usunac: ";
					cin >> x_del;
					if (Szukaj_element(tab, size, x_del, miejsce_del) != 0) {
						cout << endl << "Usunieto element " << Szukaj_element(tab, size, x_del, miejsce_del) << " z " << miejsce_del << " miejsca w tablicy haszujacej" << endl;
						tab[miejsce_del] = 0;
						zajete--;
					}
					else
						cout << endl << "W tablicy nie ma takiego elementu" << endl;
					break;
				}
					case 5: {
						char opcja;
						system("cls");
						cout << endl << "                                          ### UWAGA! ###" << endl << endl << "Uzywanie tablicy modyfikowanej w tej czesci menu moze byc zle obslugiwana w innych typach obslugi kolizji!" << endl;
						cout << "Czy chcesz wyzerowac tablice? T/N" << endl;
						cin >> opcja;
						switch (opcja) {
						case 'T':
						case 't': {
							for (int i = 0; i < size; i++) {
								tab[i] = 0;
								tab_los[i] = 0;
							}
							probkowanie = 0;
							zajete = 0;
							break;
						}
						case 'N':
						case 'n': {
							break;
						}
						}
						system("cls");
						break;
					}
				}
			} while (opt1 != 5);
			break;
		}
		case 2: {
			cout << endl << "### Podwojne Haszowanie ###" << endl;
			do{
				cout << endl;
				cout << endl << "1. Dodaj losowe elementy do tablicy #" << "                                      " << "Calkowita ilosc probek: " << probkowanie << endl;
				cout << "2. Wyswietl tablice haszujaca" << endl;
				cout << "3. Wyszukaj element" << endl;
				cout << "4. Usun element" << endl;
				cout << "5. Powrot do menu glownego" << endl;
				cout << "Podaj numer opcji, ktora cie interesuje: ";
				cin >> opt2;
				switch (opt2) {
					case 1: {
				cout << "Podaj ile elementow chcesz dodac: ";
				cin >> ilosc;
				if (size < (zajete + ilosc)) {
					cout << endl << "Nie udalo sie dodac elementow do tablicy haszujacej" << endl;
					cout << "Taka ilosc elementow, przekroczy zakres rozmiaru tablicy, ktory podales na poczatku" << endl;
					cout << "Maksymalna ilosc elementow jaka mozesz teraz dodac to " << (size - zajete) << endl;
				}
				else {
					Wylosuj_wszystko(tab_los, ilosc);
					Dodaj_Element_Double_Hash(tab, ilosc, tab_los, size);
					zajete = ilosc + zajete;
					cout << endl << "Pomyslnie dodano elementy do tablicy haszujacej" << endl;
				}
				break;
			}
					case 2: {
						cout << endl << "Tablica haszujaca" << endl;
				for (int i = 0; i < size; i++) {
					if (tab[i] != 0)
						cout << tab[i] << " ";
					else
						cout << "# ";
					cout << endl;
				}
				break;
			}
					case 3: {
				int x;
				int miejsce = 0;
				cout << "Podaj element jaki mamy znalezc: ";
				cin >> x;
				if (Szukaj_element(tab, size, x, miejsce) != 0)
					cout << endl << "W tablicy istnieje element " << Szukaj_element(tab, size, x, miejsce) << " na " << miejsce << " miejscu w tablicy haszujacej" << endl;
				else
					cout << endl << "W tablicy nie ma takiego elementu" << endl;
				break;
			}
					case 4: {
				int x_del;
				int miejsce_del = 0;
				cout << "Podaj element jaki chcesz usunac: ";
				cin >> x_del;
				if (Szukaj_element(tab, size, x_del, miejsce_del) != 0) {
					cout << endl << "Usunieto element " << Szukaj_element(tab, size, x_del, miejsce_del) << " z " << miejsce_del << " miejsca w tablicy haszujacej" << endl;
					tab[miejsce_del] = 0;
					zajete--;
				}
				else
					cout << endl << "W tablicy nie ma takiego elementu" << endl;
				break;
			}
					case 5: {
						char opcja;
						system("cls");
						cout << endl << "                                          ### UWAGA! ###" << endl  << endl << "Uzywanie tablicy modyfikowanej w tej czesci menu moze byc zle obslugiwana w innych typach obslugi kolizji!" << endl;
						cout << "Czy chcesz wyzerowac tablice? T/N" << endl;
						cin >> opcja;
						switch (opcja) {
						case 'T':
						case 't': {
							for (int i = 0; i < size; i++) {
								tab[i] = 0;
								tab_los[i] = 0;
							}
							probkowanie = 0;
							zajete = 0;
							break;
						}
						case 'N':
						case 'n': {
							break;
						}
						}
						system("cls");
						break;
					}
				}
			} while (opt2 != 5);
			break;
		}
		case 3: {
			cout << endl << "### Linkowanie ###" << endl;
			do {
				cout << endl;
				cout << endl << "1. Dodaj losowe elementy do tablicy #" << endl;
				cout << "2. Wyswietl tablice haszujaca" << endl;
				cout << "3. Wyszukaj element" << endl;
				cout << "4. Usun element" << endl;
				cout << "5. Powrot do menu glownego" << endl;
				cout << "Podaj numer opcji, ktora cie interesuje: ";
				cin >> opt3;
				switch (opt3) {
				case 1: {
					do{
					cout << "Podaj ile elementow chcesz dodac (maksymalnie na raz mozesz dodac " << losowanie << " elementow): ";
					cin >> ilosc;
					if (ilosc <= losowanie) {
						Wylosuj_wszystko(tab_los, ilosc);
						Dodaj_Element_link(tab_link, ilosc, tab_los, size);
						cout << endl << "Pomyslnie dodano elementy do tablicy haszujacej" << endl;
					}
					else
						cout << endl << "\t\t\t ### UWAGA ###" << endl << endl <<"Ilosc elementow przekroczyla wartosc maksymalna ktora mozna dodac na raz" << endl << endl;
					} while (ilosc > losowanie);
					break;
				}
				case 2: {
					cout << endl << "Tablica Haszujaca ( Miejsca w tablicy: Elementy zlinkowane z danym miejscem)" << endl;
					Wyswietl_Tablice_link(tab_link, size);
					cout << endl;
					break;
				}
				case 3: {
					int x;
					cout << "Podaj element jaki mamy znalezc: ";
					cin >> x;
					if (Szukaj_element_link(tab_link, size, x) != 0)
						cout << endl << "W tablicy istnieje element " << Szukaj_element_link(tab_link, size, x) << " na " << x%size << " miejscu w tablicy haszujacej" << endl;
					else
						cout << endl << "W tablicy nie ma takiego elementu" << endl;
					break;
				}
				case 4: {
					int x;
					cout << "Podaj element jaki mamy usunac: ";
					cin >> x;
					if (Szukaj_element_link(tab_link, size, x) == 0)
						cout << endl << "W tablicy nie ma takiego elementu" << endl;
					else {
						cout << "Usunieto element " << Szukaj_element_link(tab_link, size, x) << " na " << x%size << " miejscu w tablicy haszujacej" << endl;
						Usun_element_link(tab_link, size, x);
					}
					break;
				}
				case 5: {
					char opcja;
					system("cls");
					cout << endl << "                                          ### UWAGA! ###" << endl << endl << "Uzywanie tablicy modyfikowanej w tej czesci menu moze byc zle obslugiwana w innych typach obslugi kolizji!" << endl;
					cout << "Czy chcesz wyzerowac tablice? T/N" << endl;
					cin >> opcja;
					switch (opcja) {
					case 'T':
					case 't': {
						for (int i = 0; i < size; i++) {
							tab_link[i].clear();
						}
						break;
					}
					case 'N':
					case 'n': {
						break;
					}
					}
					system("cls");
					break;
				}
				}
				} while (opt3 != 5);
			break;
		}
		}
	} while(opt != 4);
	
	system("pause");
	delete[] tab;
	delete[] tab_los;
	for (int i = 0; i < size; i++) {
		tab_link[i].clear();
	}
	delete[] tab_link;
    return 0;
}

