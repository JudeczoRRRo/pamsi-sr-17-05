#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED
#include "stdafx.h"
#include <iostream>
#include <string>
#include <Windows.h>

using namespace std;

struct Element {
	string imie;
	string nazwisko;
	Element *nastepny;
	Element() {
		cout << "Podaj imie dodawanej osoby: ";
		cin >> imie;
		cout << "Podaj nazwisko dodawanej osoby: ";
		cin >> nazwisko;
		nastepny = NULL;
	}
	void wypisz_element() {
		cout << "Imie i Nazwisko: " << imie << " " << nazwisko << endl;
	}
};

class lista {
public:
	Element *glowa = NULL;
	void dodaj_element(Element **glowa);
	void wypisz_elementy(Element *glowa);
	bool usun_element(Element **glowa, string a);
	bool usun_elementy(Element **glowa);
	void menu_listy();
};
#endif KOLEJKA_H_INCLUDED

