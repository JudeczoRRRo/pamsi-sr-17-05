// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <conio.h>
#include <ctime>
#include <Windows.h>
#include <fstream>
#include <iostream>

using namespace std;

class Pole {
	int wartosc;
	int odwiedzone;
public:
	Pole() {
		wartosc = 0;
		odwiedzone = 0;
	}
	void set_wart(int x) {
		wartosc = x;
	}
	int get_wart() {
		return wartosc;
	}
	void set_odw(int x) {
		odwiedzone = x;
	}
	int get_odw() {
		return odwiedzone;
	}
};
void Wyswietl_Powitanie() {
	cout << endl << endl << endl << endl;
	cout << "\t\t\t\t'";
	Sleep(200);
	cout << "J";
	Sleep(200);
	cout << "U";
	Sleep(200);
	cout << "D";
	Sleep(200);
	cout << "E";
	Sleep(200);
	cout << " ";
	Sleep(200);
	cout << "G";
	Sleep(200);
	cout << "A";
	Sleep(200);
	cout << "M";
	Sleep(200);
	cout << "E";
	Sleep(200);
	cout << "S";
	Sleep(200);
	cout << "'" << endl;
	Sleep(500);
	cout << "\t\t\t\t  P";
	Sleep(200);
	cout << "R";
	Sleep(200);
	cout << "E";
	Sleep(200);
	cout << "S";
	Sleep(200);
	cout << "E";
	Sleep(200);
	cout << "N";
	Sleep(200);
	cout << "T";
	Sleep(200);
	cout << "S";
	Sleep(3000);
	system("cls");
	cout << endl << "\t\t\t\tS";
	Sleep(350);
	cout << "A";
	Sleep(350);
	cout << "P";
	Sleep(350);
	cout << "E";
	Sleep(350);
	cout << "R" << endl;
	Sleep(2000);
	cout << "\t\t\tby Mikolaj Krzyzanowski" << endl << endl;
	Sleep(2000);
	cout << "Podaj rozmiar tablicy na jakiej chcesz zaczac rozgrywke (domyslnie graj na rozmiarze 10): ";
}

void Wyswietl_Wejscie() {
	cout << "\t\t\tG";
	Sleep(200);
	cout << "E";
	Sleep(200);
	cout << "N";
	Sleep(200);
	cout << "E";
	Sleep(200);
	cout << "R";
	Sleep(200);
	cout << "O";
	Sleep(200);
	cout << "W";
	Sleep(200);
	cout << "A";
	Sleep(200);
	cout << "N";
	Sleep(200);
	cout << "I";
	Sleep(200);
	cout << "E";
	Sleep(200);
	cout << " ";
	Sleep(200);
	cout << "P";
	Sleep(200);
	cout << "L";
	Sleep(200);
	cout << "A";
	Sleep(200);
	cout << "N";
	Sleep(200);
	cout << "S";
	Sleep(200);
	cout << "Z";
	Sleep(200);
	cout << "Y";
	Sleep(500);
	cout << ".";
	Sleep(500);
	cout << ".";
	Sleep(500);
	cout << ".";
	Sleep(2000);
}

void Losuj_Bomby(Pole **tab, int ilosc_bomb, int size) {
	int x, y;
	srand(time(NULL));
	for (int i = 0; i < ilosc_bomb; i++) {
		x = rand() % ((size - 1) + 1);
		y = rand() % ((size - 1) + 1);
		if (tab[x][y].get_wart() != 9)
			tab[x][y].set_wart(9);
		else i--;
	}
}

void Odslon_Zera(Pole **tab, int y, int x, int size) {
	if (y != 0) {
		if (x != 0)
			if (tab[y - 1][x - 1].get_wart() != 0)
				tab[y - 1][x - 1].set_odw(1);
		if (tab[y - 1][x].get_wart() != 0)
			tab[y - 1][x].set_odw(1);
		if (x != size - 1)
			if (tab[y - 1][x + 1].get_wart() != 0)
				tab[y - 1][x + 1].set_odw(1);
	}
	if (x != 0)
		if (tab[y][x - 1].get_wart() != 0)
			tab[y][x - 1].set_odw(1);
	if (tab[y][x].get_wart() != 0)
		tab[y][x].set_odw(1);
	if (x != size - 1)
		if (tab[y][x + 1].get_wart() != 0)
			tab[y][x + 1].set_odw(1);
	if (y != size - 1) {
		if (x != 0)
			if (tab[y + 1][x - 1].get_wart() != 0)
				tab[y + 1][x - 1].set_odw(1);
		if (tab[y + 1][x].get_wart() != 0)
			tab[y + 1][x].set_odw(1);
		if (x != size - 1)
			if (tab[y + 1][x + 1].get_wart() != 0)
				tab[y + 1][x + 1].set_odw(1);
	}

	//      Szukanie pozostałych zer

	if (y != 0) {
		if (x != 0)
			if (tab[y - 1][x - 1].get_wart() != 0)
				if (tab[y - 1][x - 1].get_odw() == 0) {
					tab[y - 1][x - 1].set_odw(1);
					Odslon_Zera(tab, y - 1, x - 1, size);
				}

		if (tab[y - 1][x].get_wart() == 0)
			if (tab[y - 1][x].get_odw() == 0) {
				tab[y - 1][x].set_odw(1);
				Odslon_Zera(tab, y - 1, x, size);
			}
		if (x != size - 1)
			if (tab[y - 1][x + 1].get_wart() == 0)
				if (tab[y - 1][x + 1].get_odw() == 0) {
					tab[y - 1][x + 1].set_odw(1);
					Odslon_Zera(tab, y - 1, x + 1, size);
				}
	}
	if (x != 0)
		if (tab[y][x - 1].get_wart() == 0)
			if (tab[y][x - 1].get_odw() == 0) {
				tab[y][x - 1].set_odw(1);
				Odslon_Zera(tab, y, x - 1, size);
			}
	if (tab[y][x].get_wart() == 0)
		if (tab[y][x].get_odw() == 0) {
			tab[y][x].set_odw(1);
			Odslon_Zera(tab, y, x, size);
		}
	if (x != size - 1)
		if (tab[y][x + 1].get_wart() == 0)
			if (tab[y][x + 1].get_odw() == 0) {
				tab[y][x + 1].set_odw(1);
				Odslon_Zera(tab, y, x + 1, size);
			}
	if (y != size - 1) {
		if (x != 0)
			if (tab[y + 1][x - 1].get_wart() == 0)
				if (tab[y + 1][x - 1].get_odw() == 0) {
					tab[y + 1][x - 1].set_odw(1);
					Odslon_Zera(tab, y + 1, x - 1, size);
				}
		if (tab[y + 1][x].get_wart() == 0)
			if (tab[y + 1][x].get_odw() == 0) {
				tab[y + 1][x].set_odw(1);
				Odslon_Zera(tab, y + 1, x, size);
			}
		if (x != size - 1)
			if (tab[y + 1][x + 1].get_wart() == 0)
				if (tab[y + 1][x + 1].get_odw() == 0) {
					tab[y + 1][x + 1].set_odw(1);
					Odslon_Zera(tab, y + 1, x + 1, size);
				}
	}

}

int Ile_Bomb(Pole **tab, int x, int y, int size) {
	int licznik = 0;
	if (y != 0) {
		if (x != 0)
			if (tab[y - 1][x - 1].get_wart() == 9)
				licznik++;
		if (tab[y - 1][x].get_wart() == 9)
			licznik++;
		if (x != size - 1)
			if (tab[y - 1][x + 1].get_wart() == 9)
				licznik++;
	}
	if (x != 0)
		if (tab[y][x - 1].get_wart() == 9)
			licznik++;
	if (x != size - 1)
		if (tab[y][x + 1].get_wart() == 9)
			licznik++;
	if (y != size - 1) {
		if (x != 0)
			if (tab[y + 1][x - 1].get_wart() == 9)
				licznik++;
		if (tab[y + 1][x].get_wart() == 9)
			licznik++;
		if (x != size - 1)
			if (tab[y + 1][x + 1].get_wart() == 9)
				licznik++;
	}
	return licznik;
}

int main()
{
	fstream plik;
	char opt, opt_g = 't', opt_g2;
	int size, ruchy, licznik_bomb, ilosc_bomb;
	int wskaznik_x;
	int wskaznik_y;


	Wyswietl_Powitanie();
	cin >> size;
	cout << "Podaj ile bomb ma byc na planszy (dla rozmiaru 10 optymalna iloscia jest 20): ";
	cin >> ilosc_bomb;
	system("cls");


	Pole **tab = new Pole*[size];
	for (int i = 0; i < size; i++) {
		tab[i] = new Pole[size];
	}
	while (opt_g != 'n') {
		cout << endl << endl << endl;
		Wyswietl_Wejscie();
		system("cls");
		wskaznik_x = 0;
		wskaznik_y = 0;
		ruchy = 0;
		licznik_bomb = 0;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				tab[j][i].set_odw(0);
				tab[j][i].set_wart(0);
			}
		}
		Losuj_Bomby(tab, ilosc_bomb, size);
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (tab[i][j].get_wart() != 9) {
					tab[i][j].set_wart(Ile_Bomb(tab, j, i, size));
				}
			}
		}
		/*for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				cout << " . ";
				Sleep(50);
				if (i == 0 && j == size - 1) {
					cout << "\t\t Poruszaj sie po tablicy za pomoca WSAD" << endl;
					Sleep(100);
				}
				if (i == 1 && j == size - 1) {
					cout << "\t\t Klawisz E zatwierdza wybor pola" << endl;
					Sleep(100);
				}
				if (i == 2 && j == size - 1) {
					cout << "\t\t Klawisz F ustawia flage" << endl;
					Sleep(100);
				}
				if (i == 3 && j == size - 1) {
					cout << "\t\t Klawisz K konczy gre" << endl;
					Sleep(100);
				}
				if (i == 4 && j == size - 1) {
					cout << "\t\t Pole # to bomba," << endl;
					Sleep(100);
				}
				if (i == 5 && j == size - 1) {
					cout << "\t\t Pole z liczba oznacza ile bomb jest w poblizu" << endl;
					Sleep(100);
				}
				if (i == 6 && j == size - 1) {
					cout << "\t\t Pole ? to flaga" << endl;
					Sleep(100);
				}
				if (i == 8 && j == size - 1) {
					cout << "\t\t Wykonane ruchy: " << ruchy << endl;
					Sleep(100);
				}
				if (i > 6 && i != 8 && j == size - 1)
					cout << endl;
			}
		}
		Sleep(500);
		system("cls");
		*/
		opt_g2 = 'n';
		while (opt_g2 != 't') {
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					if (wskaznik_x == j && wskaznik_y == i)
						cout << "[";
					else
						cout << " ";
					if (tab[i][j].get_odw() == 0)
						cout << ".";
					else {
						if (tab[i][j].get_wart() <= 8 && tab[i][j].get_wart() >= 0)
							cout << tab[i][j].get_wart();
						if (tab[i][j].get_wart() == 9)
							cout << "#";
						if (tab[i][j].get_wart() == 10 || tab[i][j].get_wart() == 11)
							cout << "?";
					}
					if (wskaznik_x == j && wskaznik_y == i)
						cout << "]";
					else
						cout << " ";
					if (i == 0 && j == size - 1) {
						cout << "\t\t Poruszaj sie po tablicy za pomoca WSAD" << endl;
					}
					if (i == 1 && j == size - 1) {
						cout << "\t\t Klawisz E zatwierdza wybor pola" << endl;
					}
					if (i == 2 && j == size - 1) {
						cout << "\t\t Klawisz F ustawia flage" << endl;
					}
					if (i == 3 && j == size - 1) {
						cout << "\t\t Klawisz K konczy gre" << endl;
					}
					if (i == 4 && j == size - 1) {
						cout << "\t\t Pole # to bomba," << endl;
					}
					if (i == 5 && j == size - 1) {
						cout << "\t\t Pole z liczba oznacza ile bomb jest w poblizu" << endl;
					}
					if (i == 6 && j == size - 1) {
						cout << "\t\t Pole ? to flaga" << endl;
					}
					if (i == 8 && j == size - 1) {
						cout << "\t\t Wykonane ruchy: " << ruchy << endl;
					}
					if (i > 6 && i != 8 && j == size - 1)
						cout << endl;
				}
			}
			opt = _getch();
			if (opt == 'w') {
				if (wskaznik_y != 0) {
					wskaznik_y--;
				}
			}
			if (opt == 's') {
				if (wskaznik_y != size - 1) {
					wskaznik_y++;
				}
			}
			if (opt == 'a') {
				if (wskaznik_x != 0) {
					wskaznik_x--;
				}
			}
			if (opt == 'd') {
				if (wskaznik_x != size - 1) {
					wskaznik_x++;
				}
			}
			if (opt == 'e') {
				if (tab[wskaznik_y][wskaznik_x].get_odw() != 1) {
					tab[wskaznik_y][wskaznik_x].set_odw(1);
					if (tab[wskaznik_y][wskaznik_x].get_wart() == 0) {
						Odslon_Zera(tab, wskaznik_y, wskaznik_x, size);
					}
					if (tab[wskaznik_y][wskaznik_x].get_wart() == 9) {
						system("cls");
						for (int i = 0; i < size; i++) {
							for (int j = 0; j < size; j++) {
								if (wskaznik_x == j && wskaznik_y == i)
									cout << "[";
								else
									cout << " ";
								if (tab[i][j].get_wart() == 9)
									cout << "#";
								if (tab[i][j].get_odw() == 0 && tab[i][j].get_wart() != 9)
									cout << ".";
								else {
									if (tab[i][j].get_wart() <= 8 && tab[i][j].get_wart() >= 0)
										cout << tab[i][j].get_wart();
									if (tab[i][j].get_wart() == 10)
										cout << "!";
									if (tab[i][j].get_wart() == 11)
										cout << "%";
								}
								if (wskaznik_x == j && wskaznik_y == i)
									cout << "]";
								else
									cout << " ";
								if (i == 0 && j == size - 1)
									cout << "\t\tPole % oznacza ze ustawiono flage na bombie" << endl;
								if (i == 1 && j == size - 1)
									cout << "\t\tPole ! oznacza ze ustawiono flage niepotrzebnie" << endl;
								if (i == 2 && j == size - 1)
									cout << "\t\tPole # oznacza reszte bomb na polu" << endl;
								if (i == 3 && j == size - 1)
									cout << "\t\tPole [#] oznacza bombe ktora wybuchla" << endl;
								if (i > 3 && j == size - 1)
									cout << endl;
							}
						}
						cout << endl << endl << endl << "\t\t### TRAFILES BOMBE, KONIEC GRY ###" << endl << endl;
						system("pause");
						system("cls");
						cout << "Czy chcesz rozpoczac od nowa?" << endl;
						cout << "T - rozpoczecie od nowa, N - Wyjscie z gry" << endl;
						opt_g = _getch();
						opt_g2 = 't';
					}
					ruchy++;
				}
			}
			if (opt == 'f') {
				if (tab[wskaznik_y][wskaznik_x].get_odw() != 1) {
					ruchy++;
					tab[wskaznik_y][wskaznik_x].set_odw(1);
					if (tab[wskaznik_y][wskaznik_x].get_wart() == 9) {
						licznik_bomb++;
						tab[wskaznik_y][wskaznik_x].set_wart(11);
					}
					else
						tab[wskaznik_y][wskaznik_x].set_wart(10);
					if (licznik_bomb == ilosc_bomb) {
						system("cls");
						for (int i = 0; i < size; i++) {
							for (int j = 0; j < size; j++) {
								cout << " ";
								if (tab[i][j].get_odw() == 0)
									cout << ".";
								else {
									if (tab[i][j].get_wart() <= 8 && tab[i][j].get_wart() >= 0)
										cout << tab[i][j].get_wart();
									if (tab[i][j].get_wart() == 9)
										cout << "#";
									if (tab[i][j].get_wart() == 10)
										cout << "!";
									if (tab[i][j].get_wart() == 11) {
										cout << "%";
									}
								}
								cout << " ";
								if (i == 0 && j == size - 1)
									cout << "\t\tPole % oznacza ze ustawiono flage na bombie" << endl;
								if (i == 1 && j == size - 1)
									cout << "\t\tPole ! oznacza ze ustawiono flage niepotrzebnie" << endl;
								if (i > 1 && j == size - 1)
									cout << endl;
							}
						}
						cout << endl << endl << endl << "\t\t### ZNALAZLES WSZYSTKIE BOMBY ###" << endl << "\t\t UKONCZYLES GRE Z WYNIKIEM:" << ruchy << endl;
						system("pause");
						system("cls");
						cout << "Czy chcesz rozpoczac od nowa?" << endl;
						cout << "T - rozpoczecie od nowa, N - Wyjscie z gry" << endl;
						opt_g = _getch();
						opt_g2 = 't';
					}
				}
			}
			if (opt == 'k') {
				system("cls");
				cout << "Czy chcesz rozpoczac od nowa?" << endl;
				cout << "T - rozpoczecie od nowa, N - Wyjscie z gry" << endl;
				opt_g = _getch();
				opt_g2 = 't';
			}
			system("cls");
		}
	}
system("cls");
cout << "Wychodzisz z gry" << endl;
system("pause");
return 0;
}


