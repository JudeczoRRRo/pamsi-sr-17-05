// Lab2.3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;
int rozmiar = 3, ile, glowa, ogon;
int* dane = new int[rozmiar];
void wyswietl_kolejke() {
	if (ile == 0) {
		cout << "Kolejka jest pusta" << endl;
	}
	else {
		int indeks;
		for (int i = 0; i < ile; i++) {
			indeks = glowa + i;
			if (glowa + i >= 100) indeks = glowa + i - 100;
			cout << dane[indeks] << " ";
		}
	}
}
void wstaw() {
	if (ile == 3) {
		int* temp = new int[rozmiar];
		for (int i = 0; i < ile; i++) {
			temp[i] = dane[i];
		}
		delete[] dane;
		rozmiar = rozmiar + 3;
		int* dane = new int[rozmiar];
		for (int i = 0; i < ile; i++) {
			dane[i] = temp[i];
		}
		delete[] temp;
	}
	else {
		cout << "Podaj liczbe, wstawimy ja do kolejki: ";
		cin >> dane[ogon];
		ogon = (ogon + 1) % 100;
		ile = ile + 1;
	}
}
void usun_elem() {
	if (ile == 0) {
		cout << "Kolejka jest juz pusta, najpierw dodaj element do kolejki!" << endl;
	}
	else {
		cout << "Usuwamy liczbe: " << dane[glowa] << " z kolejki" << endl;
		glowa = (glowa + 1) % 100;
		ile = ile - 1;

	}
}
void usun_all() {
	if (ile == 0) {
		cout << "Kolejka jest juz pusta, najpierw dodaj element do kolejki!" << endl;
	}
	else {
		for (int i = ile; i > 0; i--) {
			usun_elem();
		}
	}
}
int main()
{
	int opt;
	ile = 0;
	glowa = 0;
	ogon = 0;
	do {
		cout << endl;
		cout << "1. Dodaj element do kolejki"<< endl;
		cout << "2. Usun jeden element z kolejki"<<endl;
		cout << "3. Wyswietl kolejke" << endl;
		cout << "4. Usun wszystkie elementy z kolejki" << endl;
		cout << "5. Zamknij program" << endl;
		cout << "Podaj numer opcji ktora cie interesuje: ";
		cin >> opt;
		switch (opt) {
		case 1: wstaw(); break;
		case 2: usun_elem(); break;
		case 3: cout << "Kolejka : "; wyswietl_kolejke(); break;
		case 4: usun_all(); break;
		case 5: delete[] dane;  break;
		}
	} while (opt != 5);
    return 0;
}

