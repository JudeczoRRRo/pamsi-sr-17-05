// Lab4.3.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>

using namespace std;

template <typename T>
class TreeNode {
public:
	TreeNode <T> *father;
	TreeNode <T> *sons[3];
	int much_sons;
	T element;
	TreeNode(T elem) { 
		father = NULL; 
		sons[0] = NULL; 
		much_sons = 0;
		element = elem;
	}
	~TreeNode() {}
};
template <typename T>
class Tree {
	TreeNode <T> *root;
	int height;
public:
	Tree() : root(NULL), height(0) {}
	~Tree() {}
	void Add_Node(TreeNode <T> * aNode, TreeNode <T> * pNode) {
		aNode->father = pNode; // Ustawienie ojca dodawanego wezla

		if (!pNode) { // Jesli dodawany wezel nie ma ojca, dodawany jest jako korzen
			root = aNode;
			height++;
		}

		else { // Jesli dodwanay element ma ojca
			if (pNode->much_sons == 2){
				cout << "Limit dla tego drzewa to 2 synow na jednego ojca, nie przesadzajmy..." << endl;
			}
			else if (pNode->much_sons==0) { // Jesli ojciec nie ma syna
				pNode->sons[pNode->much_sons] = aNode; // Dodajemy wezel jako syna
				if (!pNode->sons[1]) { // Jesli break prawego syna, zwiekszamy wysokosc
					height++;
					pNode->much_sons++;
				}
			}

			else if (pNode->sons[pNode->much_sons] && !pNode->sons[pNode->much_sons+1]) { // Jesli ojciec ma lewego syna i prawego nie
				pNode->sons[pNode->much_sons+1] = aNode; //Dodajemy wezel jako prawego syna
				if (!pNode->sons[pNode->much_sons]) {
					height++; // Jesli brak drugiego syna zwieksz wysokosc
					pNode->much_sons++;
				}
			}
		}

	}
	void Delete_Node(TreeNode <T> * dNode) {
		TreeNode <T> *temp_1 = NULL; // Tworzenie dwoch obiektow tymczasowych 
		TreeNode <T> *temp_2 = NULL;

		if (!dNode->sons[dNode->much_sons] || !dNode->sons[dNode->much_sons+1]) { // Jesli wezel nie ma synow lub ma tylko jednego
			if (dNode->sons[dNode->much_sons]) { // Jesli lewy syn istnieje
				temp_1 = dNode->sons[dNode->much_sons+1]; // Ustaw obiekt tymczasowy na lewego syna usuwanego wezla
			}
			else { // Jesli prawy syn istnieje
				temp_1 = dNode->sons[dNode->much_sons+1]; // Ustaw obiekt tymczasowy na prawego syna usuwanego wezla
			}
		}
		else { // Jesli wezel ma oboje synow
			if (dNode->sons[dNode->much_sons+1]) { // Jesli prawy syn istnieje
				temp_1 = dNode->sons[dNode->much_sons+1]; // Ustaw obiekt tymczasowy na prawego syna usuwanego wezla
				while (temp_1->sons[dNode->much_sons]) {// Szukaj wezla ktory nie ma lewego syna
					temp_1 = temp_1->sons[dNode->much_sons]; // Zapisz go do obiektu tymczasowego
				}
			}
			else if (!dNode->sons[dNode->much_sons]+1 && dNode->sons[dNode->much_sons]) { // jesli prawe dziecko nie istnieje a prawe tak
				temp_1 = dNode; // Ustaw obiekt tymczasowy aby wskazywal na usuwany wezel
				temp_2 = temp_1->sons[dNode->much_sons]; // Ustaw drugi obiekt tymczasowy aby wskazywal na lewe dziecko usuwanego wezla
				while (temp_2 && (temp_1 == temp_2->sons[dNode->much_sons+1])) { // idac w gore drzewa znajdz wezel dla ktorego usuwany wezel jest w lewej galezi
					temp_1 = temp_2;
					temp_2 = temp_2->father;
				}
			}
		}
		if (temp_1) { // Jesli dziecko istnieje
			temp_1->father = dNode->father; // to jego ojcem staje sie jego dziadek
		}
		if (dNode == root) { // Jesli usuwany wezel jest korzeniem
			root = temp_1; // Korzeniem staje sie syn lub korzen jest usuwany jesli dziecko nie istnieje
			height--;
			dNode->much_sons--;
		}
		else if (dNode->father->sons[dNode->much_sons] == dNode) { // Jesli usuwany wezel jest lewym synem ojca
			dNode->father->sons[dNode->much_sons] = temp_1; // to syn usuwanego wezla jest teraz lewym synem dziadka lub jest usuwany jesli syn nie istnieje
			if (!dNode->father->sons[dNode->much_sons+1] &&height) {
				height--;
				dNode->much_sons--;
			}
		}
		else {// Jesli usuwany wezel jest prawym synem ojca
			dNode->father->sons[dNode->much_sons+1] = temp_1; // to syn usuwanego wezla jest teraz prawym synem dziadka lub jest usuwany jesli syn nie istnieje
			if (!dNode->father->sons[dNode->much_sons] && height) {
				height--;
				dNode->much_sons--;
			}
		}
		if (dNode != temp_1 && temp_1) {
			dNode->element = temp_1->element; // Kopiowanie danych
		}
		else if (!temp_1) {
			dNode->element = NULL;
		}
	}
	T Show_Root() {
		if (root)
			return root->element;
	}
	int Show_Height() {
		return height;
	}
};
int main()
{
	int elem_1 = 1;
	int elem_2 = 2;
	int elem_3 = 3;
	int elem_4 = 4;
	Tree <int> tree;
	TreeNode <int> node_1(elem_1);
	TreeNode <int> node_2(elem_2);
	TreeNode <int> node_3(elem_3);
	TreeNode <int> node_4(elem_4);
	cout << endl << "### Prezentacja dzialania programu: Drzewo ogolne ###" << endl;
	Sleep(2000);
	cout << endl << "Dodane galezi do drzewa o wartosciach: " << endl << "1. " << elem_1 << endl << "2. " << elem_2 << endl << "3. " << elem_3 << endl << "4. " << elem_4 << endl;
	Sleep(4000);
	cout << endl << "Parametry drzewa przy starcie programu: " << endl;
	Sleep(2000);
	cout << endl << "Korzen: NULL" << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Dodanie pierwszej galezi o wartosci: " << elem_1 << endl;
	Sleep(1500);
	tree.Add_Node(&node_1, NULL);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Dodanie do pierwszej galezi, galezi drugiej o wartosci: " << elem_2 << endl;
	Sleep(1500);
	tree.Add_Node(&node_2, &node_1);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Dodanie do pierwszej galezi, galezi trzeciej o wartosci: " << elem_3 << endl;
	Sleep(1500);
	tree.Add_Node(&node_3, &node_1);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Dodanie do pierwszej galezi, galezi czwartej o wartosci: " << elem_4 << endl << endl;
	Sleep(1500);
	tree.Add_Node(&node_4, &node_1);
	Sleep(1500);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Dodanie do drugiej galezi, galezi czwartej o wartosci: " << elem_4 << endl;
	Sleep(1500);
	tree.Add_Node(&node_4, &node_2);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Usuniecie drugiej galezi" << endl;
	Sleep(1500);
	tree.Delete_Node(&node_2);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Usuniecie czwartej galezi" << endl;
	Sleep(1500);
	tree.Delete_Node(&node_4);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Usuniecie trzeciej galezi" << endl;
	Sleep(1500);
	tree.Delete_Node(&node_3);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Usuniecie pierwszej galezi (korzen)" << endl;
	Sleep(1500);
	tree.Delete_Node(&node_1);
	cout << endl << "Parametry drzewa po usunieciu wszystkich galezi" << endl;
	cout << endl << "Korzen: NULL" << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << "### Koniec prezentacji dzialania programu" << endl;
	system("PAUSE");
	return 0;
}
