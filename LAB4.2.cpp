// LAB4.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>

using namespace std;

template <typename T>
class B_TreeNode {
public:
	B_TreeNode <T> *father, *left_son, *right_son;
	T element;
	B_TreeNode(T elem): father(NULL), left_son(NULL), right_son(NULL), element(elem) {}
	~B_TreeNode() {}
};
template <typename T>
class B_Tree {
	B_TreeNode <T> *root;
	int height;
public:
	B_Tree(): root(NULL),height(0){}
	~B_Tree() {}
	void Add_Node(B_TreeNode <T> * aNode, B_TreeNode <T> * pNode) {
		aNode->father = pNode; // Ustawienie ojca dodawanego wezla

		if (!pNode) { // Jesli dodawany wezel nie ma ojca, dodawany jest jako korzen
			root = aNode;
			height++;
		}

		else { // Jesli dodwanay element ma ojca
			if (!pNode->left_son) { // Jesli ojciec nie ma lewego syna
				pNode->left_son = aNode; // Dodajemy wezel jako lewego syna
				if (!pNode->right_son) // Jesli break prawego syna, zwiekszamy wysokosc
					height++;
			}

			else if (pNode->left_son && !pNode->right_son) { // Jesli ojciec ma lewego syna i prawego nie
				pNode->right_son = aNode; //Dodajemy wezel jako prawego syna
				if (!pNode->left_son)
					height++; // Jesli brak drugiego syna zwieksz wysokosc
			}

			else // jesli ojciec ma oboje synow
				cout << "Nie mozna dodac nastepnego elementu dla tego wezla!" << endl;
		}

	}
	void Delete_Node(B_TreeNode <T> * dNode) {
		B_TreeNode <T> *temp_1 = NULL; // Tworzenie dwoch obiektow tymczasowych 
		B_TreeNode <T> *temp_2 = NULL;

		if (!dNode->left_son || !dNode->right_son) { // Jesli wezel nie ma synow lub ma tylko jednego
			if (dNode->left_son) { // Jesli lewy syn istnieje
				temp_1 = dNode->left_son; // Ustaw obiekt tymczasowy na lewego syna usuwanego wezla
			}
			else { // Jesli prawy syn istnieje
				temp_1 = dNode->right_son; // Ustaw obiekt tymczasowy na prawego syna usuwanego wezla
			}
		}
		else { // Jesli wezel ma oboje synow
			if (dNode->right_son) { // Jesli prawy syn istnieje
				temp_1 = dNode->right_son; // Ustaw obiekt tymczasowy na prawego syna usuwanego wezla
				while (temp_1->left_son) {// Szukaj wezla ktory nie ma lewego syna
					temp_1 = temp_1->left_son; // Zapisz go do obiektu tymczasowego
				}
			}
			else if (!dNode->right_son && dNode->left_son) { // jesli prawe dziecko nie istnieje a prawe tak
				temp_1 = dNode; // Ustaw obiekt tymczasowy aby wskazywal na usuwany wezel
				temp_2 = temp_1->left_son; // Ustaw drugi obiekt tymczasowy aby wskazywal na lewe dziecko usuwanego wezla
				while (temp_2 && (temp_1 == temp_2->right_son)) { // idac w gore drzewa znajdz wezel dla ktorego usuwany wezel jest w lewej galezi
					temp_1 = temp_2;
					temp_2 = temp_2->father;
				}
			}
		}
		if (temp_1) { // Jesli dziecko istnieje
			temp_1->father = dNode->father; // to jego ojcem staje sie jego dziadek
		}
		if (dNode == root) { // Jesli usuwany wezel jest korzeniem
			root = temp_1; // Korzeniem staje sie syn lub korzen jest usuwany jesli dziecko nie istnieje
			height--;
		}
		else if (dNode->father->left_son == dNode) { // Jesli usuwany wezel jest lewym synem ojca
			dNode->father->left_son = temp_1; // to syn usuwanego wezla jest teraz lewym synem dziadka lub jest usuwany jesli syn nie istnieje
			if (!dNode->father->right_son&&height) {
				height--;
			}
		}
		else {// Jesli usuwany wezel jest prawym synem ojca
			dNode->father->right_son = temp_1; // to syn usuwanego wezla jest teraz prawym synem dziadka lub jest usuwany jesli syn nie istnieje
			if (!dNode->father->left_son && height) {
				height--;
			}
		}
		if (dNode != temp_1 && temp_1) {
			dNode->element = temp_1->element; // Kopiowanie danych
		}
		else if (!temp_1) {
			dNode->element = NULL;
		}
	}
	T Show_Root() {
		if (root)
			return root->element;
	}
	int Show_Height() {
		return height;
	}
	/*void Show_Tree(B_TreeNode <T> * sNode) {
		if (!root) { // Jesli drzewo nie ma korzenia
			cout << "Drzewo jest puste" << endl; // Wyswietla ze drzewo jest puste
		}
		else {
			cout << sNode->father->element;
			if (sNode->left_son) { // Jesli ma lewego syna
				sNode = sNode->left_son;
				Show_Tree(sNode);
			}
			if (sNode->left_son && sNode->right_son) {
				sNode = sNode->right_son;
				Show_Tree(sNode);
			}
		}
	}*/
};
int main()
{
	int elem_1 = 1;
	int elem_2 = 2;
	int elem_3 = 3;
	int elem_4 = 4;
	B_Tree <int> tree;
	B_TreeNode <int> node_1(elem_1);
	B_TreeNode <int> node_2(elem_2);
	B_TreeNode <int> node_3(elem_3);
	B_TreeNode <int> node_4(elem_4);
	cout << endl << "### Prezentacja dzialania programu: Drzewo binarne ###" << endl;
	Sleep(2000);
	cout << endl << "Dodane galezi do drzewa o wartosciach: " << endl << "1. " << elem_1 << endl << "2. " << elem_2 << endl << "3. " << elem_3 << endl << "4. " << elem_4 << endl;
	Sleep(4000);
	cout << endl << "Parametry drzewa przy starcie programu: " << endl;
	Sleep(2000);
	cout << endl << "Korzen: NULL"  << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Dodanie pierwszej galezi o wartosci: " << elem_1 << endl;
	Sleep(1500);
	tree.Add_Node(&node_1, NULL);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Dodanie do pierwszej galezi, galezi drugiej o wartosci: " << elem_2 << endl;
	Sleep(1500);
	tree.Add_Node(&node_2, &node_1);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Dodanie do pierwszej galezi, galezi trzeciej o wartosci: " << elem_3 << endl;
	Sleep(1500);
	tree.Add_Node(&node_3, &node_1);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Dodanie do pierwszej galezi, galezi czwartej o wartosci: " << elem_4 << endl << endl;
	Sleep(1500);
	tree.Add_Node(&node_4, &node_1);
	Sleep(1500);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Dodanie do drugiej galezi, galezi czwartej o wartosci: " << elem_4 << endl;
	Sleep(1500);
	tree.Add_Node(&node_4, &node_2);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Usuniecie czwartej galezi" << endl;
	Sleep(1500);
	tree.Delete_Node(&node_4);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Usuniecie trzeciej galezi" << endl;
	Sleep(1500);
	tree.Delete_Node(&node_3);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Usuniecie drugiej galezi" << endl;
	Sleep(1500);
	tree.Delete_Node(&node_2);
	cout << endl << "Korzen: " << tree.Show_Root() << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << endl << "Usuniecie pierwszej galezi (korzen)" << endl;
	Sleep(1500);
	tree.Delete_Node(&node_1);
	cout << endl << "Parametry drzewa po usunieciu wszystkich galezi" << endl;
	cout << endl << "Korzen: NULL" << endl;
	cout << "Wysokosc drzewa " << tree.Show_Height() << endl;
	Sleep(3000);
	cout << "### Koniec prezentacji dzialania programu" << endl;
	system("PAUSE");
    return 0;
}

