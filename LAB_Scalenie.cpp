// LAB_Scalenie.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int *pom;

void Scal(int tab[], int lewy, int srodek, int prawy) {
	int i = lewy, j = srodek + 1;
	for (int i = lewy; i <= prawy; i++)
		pom[i] = tab[i];
	for (int k = lewy; k <= prawy; k++) {
		if (i <= srodek)
			if (j <= prawy)
				if (pom[j] < pom[i])
					tab[k] = pom[j++];
				else
					tab[k] = pom[i++];
			else
				tab[k] = pom[i++];
		else
			tab[k] = pom[j++];
	}
}

void Sortowanie_Przez_Scalenie(int tab[], int lewy, int prawy) {
	if (prawy <= lewy)
		return;
	int srodek = (prawy + lewy) / 2;
	Sortowanie_Przez_Scalenie(tab, lewy, srodek);
	Sortowanie_Przez_Scalenie(tab, srodek + 1, prawy);

	Scal(tab, lewy, srodek, prawy);
}

void Losuj(int tab[], int size, int kiedy) {
	srand(time(NULL));
	for (int i = kiedy; i < size; i++) {
		tab[i] = rand();
	}
}

void Wyswietl_Tablice(int tab[], int size) {
	for (int i = 0; i < size; i++) {
		cout << tab[i] << " ";
	}
	cout << endl;
}

int main()
{
	clock_t czas_los, czas_25_los, czas_50_los, czas_75_los, czas_95_los, czas_99_los, czas_997_los, czas_odwr_los;
	clock_t start_los, start_25_los, start_50_los, start_75_los, start_95_los, start_99_los, start_997_los, start_odwr_los;
	int size;
	cout << endl << "####    SORTOWANIE PRZEZ SCALENIE    ####" << endl << endl;
	cout << "Podaj rozmiar tablicy: ";
	cin >> size;
	int *tab = new int[size];
	pom = new int[size];
	Losuj(tab, size, 0);
	Sortowanie_Przez_Scalenie(tab, 0, size - 1);
	Losuj(tab, size, 0.40*size);
	int param = size - 1000;

	for (int i = param; i < size; i++) {
		cout << tab[i] << " ";
	}
	//Wyswietl_Tablice(tab, size);
	start_los = clock();
	Sortowanie_Przez_Scalenie(tab, 0, size - 1);
	czas_los = clock() - start_los;
	cout << endl << endl << endl << endl << "POSORTOWANE TABLICA" << endl << endl << endl;
	for (int i =param; i < size; i++) {
		cout << tab[i] << " ";
	}

	//	SORTOWANIE PELNEJ LOSOWEJ TABLICY

	Losuj(tab, size, 0);
	start_los = clock();
	Sortowanie_Przez_Scalenie(tab, 0, size - 1);
	czas_los = clock() - start_los;
	cout << endl << "Posortowano pelna tablice..." << endl;

	//	SORTOWANIE 25% PELNEJ LOSOWEJ TABLICY

	Losuj(tab, size, 0.25*size);
	start_25_los = clock();
	Sortowanie_Przez_Scalenie(tab, 0, size - 1);
	czas_25_los = clock() - start_25_los;

	cout << endl << "Posortowano w 25% posortowana tablice..." << endl;

	//	SORTOWANIE 50% PELNEJ LOSOWEJ TABLICY

	Losuj(tab, size, 0.5*size);
	start_50_los = clock();
	Sortowanie_Przez_Scalenie(tab, 0, size - 1);
	czas_50_los = clock() - start_50_los;

	cout << endl << "Posortowano w 50% posortowana tablice..." << endl;

	//	SORTOWANIE 75% PELNEJ LOSOWEJ TABLICY

	Losuj(tab, size, 0.75*size);
	start_75_los = clock();
	Sortowanie_Przez_Scalenie(tab, 0, size - 1);
	czas_75_los = clock() - start_75_los;

	cout << endl << "Posortowano w 75% posortowana tablice..." << endl;

	//	SORTOWANIE 95% PELNEJ LOSOWEJ TABLICY

	Losuj(tab, size, 0.95*size);
	start_95_los = clock();
	Sortowanie_Przez_Scalenie(tab, 0, size - 1);
	czas_95_los = clock() - start_95_los;

	cout << endl << "Posortowano w 95% posortowana tablice..." << endl;

	//	SORTOWANIE 99% PELNEJ LOSOWEJ TABLICY

	Losuj(tab, size, 0.99*size);
	start_99_los = clock();
	Sortowanie_Przez_Scalenie(tab, 0, size - 1);
	czas_99_los = clock() - start_99_los;

	cout << endl << "Posortowano w 99% posortowana tablice..." << endl;

	//	SORTOWANIE 99,7% PELNEJ LOSOWEJ TABLICY

	Losuj(tab, size, 0.997*size);
	start_997_los = clock();
	Sortowanie_Przez_Scalenie(tab, 0, size - 1);
	czas_997_los = clock() - start_997_los;

	cout << endl << "Posortowano w 99,7% posortowana tablice..." << endl;

	//	SORTOWANIE ODWROTNIE POSORTOWANEJ TABLICY

	for (int i = 0; i <= size - 1; i++) {
		tab[i] = size - 1 - i;
	}
	start_odwr_los = clock();
	Sortowanie_Przez_Scalenie(tab, 0, size - 1);
	czas_odwr_los = clock() - start_odwr_los;

	cout << endl << "Posortowano odwrotnie posortowana tablice..." << endl << endl;

	printf("Czas wykonywania dla zadanej tablicy: %lu [ms] o rozmiarze %d\n", czas_los, size);
	printf("Czas wykonywania dla calej losowej tablicy: %lu [ms] o rozmiarze %d\n", czas_los, size);
	printf("Czas wykonywania dla 25/100 posortowanych elementow tablicy: %lu [ms] o rozmiarze %d\n", czas_25_los, size);
	printf("Czas wykonywania dla 50/100 posortowanych elementow tablicy: %lu [ms] o rozmiarze %d\n", czas_50_los, size);
	printf("Czas wykonywania dla 75/100 posortowanych elementow tablicy: %lu [ms] o rozmiarze %d\n", czas_75_los, size);
	printf("Czas wykonywania dla 95/100 posortowanych elementow tablicy: %lu [ms] o rozmiarze %d\n", czas_95_los, size);
	printf("Czas wykonywania dla 99/100 posortowanych elementow tablicy: %lu [ms] o rozmiarze %d\n", czas_99_los, size);
	printf("Czas wykonywania dla 997/1000 posortowanych elementow tablicy: %lu [ms] o rozmiarze %d\n", czas_997_los, size);
	printf("Czas wykonywania dla calej odwrotnie posortowanej tablicy: %lu [ms] o rozmiarze %d\n", czas_odwr_los, size);
	

	system("pause");
	return 0;

}

