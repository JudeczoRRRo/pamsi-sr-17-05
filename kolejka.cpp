#include "stdafx.h"
#include "kolejka.h"

void kolejka::wyswietl_kolejke() {
	if (ile == 0) {
		cout << "Kolejka jest pusta" << endl;
	}
	else {
		int indeks;
		for (int i = 0; i < ile; i++) {
			indeks = glowa + i;
			if (glowa + i >= 100) indeks = glowa + i - 100;
			cout << dane[indeks] << " ";
		}
	}
}
void kolejka::wstaw() {
	if (ile >= 100) {
		cout << "Kolejka jest juz pelna, najpierw usun element z kolejki!" << endl;
	}
	else {
		cout << "Podaj liczbe, wstawimy ja do kolejki: ";
		cin >> dane[ogon];
		ogon = (ogon + 1) % 100;
		ile = ile + 1;
	}
}
void kolejka::usun_element() {
	if (ile == 0) {
		cout << "Kolejka jest juz pusta, najpierw dodaj element do kolejki!" << endl;
	}
	else {
		cout << "Usuwamy liczbe: " << dane[glowa] << " z kolejki" << endl;
		glowa = (glowa + 1) % 100;
		ile = ile - 1;

	}
}
void kolejka::usun_all() {
	if (ile == 0) {
		cout << "Kolejka jest juz pusta, najpierw dodaj element do kolejki!" << endl;
	}
	else {
		for (int i = ile; i > 0; i--) {
			kolejka::usun_element();
		}
	}
}
void kolejka::menu_kolejki() {
	int opt = 0;
	do{
	cout << endl;
	cout << "%%%%% MENU KOLEJKI %%%%%" << endl;
	cout << "1. Dodaj element do kolejki" << endl;
	cout << "2. Usun jeden element z kolejki" << endl;
	cout << "3. Wyswietl kolejke" << endl;
	cout << "4. Usun wszystkie elementy z kolejki" << endl;
	cout << "5. Zamknij program" << endl;
	cout << "Podaj numer opcji ktora cie interesuje: ";
	cin >> opt;
	system("cls");
	switch (opt) {
	case 1: kolejka::wstaw(); cout << "Dodano element do kolejki" << endl; Sleep(2000); break;
	case 2: kolejka::usun_element(); cout << "Usunieto element z kolejki" << endl; Sleep(2000); break;
	case 3: cout << "Kolejka : "; kolejka::wyswietl_kolejke(); Sleep(5000); break;
	case 4: kolejka::usun_all(); cout << "Usunieto wszystko z kolejki" << endl; Sleep(2000); break;
	case 5: break;
	}
	system("cls");
} while (opt != 5);
}