#pragma once

template <typename Object>

class Queue {
private:
	int front;
	int rear;
	Object *q;
	int size_q;
public:
	Queue(int s) {
		size_q = s;
		q = new Object[size_q];
		front = 0;
		rear = 0;
	}
	int size();
	bool isEmpty();
	Object& queue_front();
	void enqueue(Object o);
	Object dequeue();
	void show();
	Object dequeue_all();
};

template<typename Object>
inline int Queue<Object>::size()
{
	return (size_q - (front - rear))%size_q;
}

template<typename Object>
inline bool Queue<Object>::isEmpty()
{
	return (front == rear);
}

template<typename Object>
inline Object & Queue<Object>::queue_front()
{
	return front;
}

template<typename Object>
inline void Queue<Object>::enqueue(Object o)
{
	if (size() == size_q - 1) {
		Object *temp = new Object[size_q * 2];
		for (int i = 0; i < size_q; i++) {
			temp[i] = q[i];
		}
		q = temp;
		size_q = size_q * 2;
	}
	
	q[rear] = o;
	rear = (rear + 1);	
}

template<typename Object>
inline Object Queue<Object>::dequeue()
{
	
	if (isEmpty()) {
		cout << "Kolejka jest pusta";
	}
	else {
		Object temp;
		temp = q[front];
		front = (front + 1);
		return temp;
	}
	
}

template<typename Object>
inline void Queue<Object>::show()
{
	if (isEmpty()) {
		cout << "Kolejka jest pusta" << endl;
	}
	else {
		for (int i = front; i < rear; i++) {
			cout << q[i] << " ";
		}
	}
	cout << "Rozmiar: " << size_q;
	cout << "Size: " << size();
}

template<typename Object>
inline Object Queue<Object>::dequeue_all()
{
	for (int i = front; i < rear; i++) {
		dequeue();
	}
	return front = 0, rear = 0;
}

