
#include "stdafx.h"
#include <iostream>
#include <string>
#include <Windows.h>
using namespace std;
int ktory;

struct Element {
	string imie;
	string nazwisko;
	
	Element *nastepny;
	Element() {
		cout << "Podaj imie dodawanej osoby: ";
		cin >> imie;
		cout << "Podaj nazwisko dodawanej osoby: ";
		cin >> nazwisko;
		nastepny = NULL;
		//ktory++;
	}
	void wypisz_element() {
		cout << "Imie i Nazwisko: " << imie << " " << nazwisko << endl;
	}
};

void dodaj_element(Element **glowa) {
	Element *nowa = new Element;
	Element *pomocniczy = (*glowa);
	Element *pomocniczy1 = NULL; //Wskazniki ktore pomoga nam sie poruszac po liscie;
	while (pomocniczy != NULL && (pomocniczy->nazwisko.compare(nowa->nazwisko) == -1)){
		pomocniczy1 = pomocniczy; // pomocniczy1 -> zawsze wskazuje na poprzedni element
		pomocniczy = pomocniczy->nastepny; // 
	}
	if (pomocniczy != NULL && (pomocniczy->nazwisko).compare(nowa->nazwisko) == 0) // element z podanym nazwiskiem juz istanieje
	{
		cout << endl << endl << "Element o nazwisku " << nowa->nazwisko << " juz istnieje, nowy musi zostac usuniety." << endl << "Jesli jest to inna osoba dodaj do nazwiska na przyklad *.2" << endl;
		delete nowa; // usuniecie obiektu
	}
	else {
		if ((*glowa) == NULL || (pomocniczy == (*glowa) && (pomocniczy->nazwisko).compare(nowa->nazwisko) == 1)) {
			nowa->nastepny = (*glowa);
			(*glowa) = nowa; // Nowy element bedzie poczatkiem listy
			ktory++;
		}
		else {
			pomocniczy1->nastepny = nowa;
			nowa->nastepny = pomocniczy;
		}
	}
}
void wypisz_elementy(Element *glowa) {
	cout << "Zawartosc listy; \n";
	cout << "Numer elementu: " << ktory;
	
	while (glowa != NULL) {
		glowa->wypisz_element(); // wypisanie jednego elementu
		glowa = glowa->nastepny; // przejscie na nastepny element
	}
}
bool usun_element(Element **glowa, string a) {
	if ((*glowa) == NULL) {
		return false;
	}// Lista jest pusta
	Element *pomocniczy = (*glowa);
	Element *pomocniczy1 = NULL;
	while (pomocniczy != NULL && (pomocniczy->nazwisko).compare(a) != 0) {
		pomocniczy1 = pomocniczy;
		pomocniczy = pomocniczy->nastepny;
	}// Szukanie wprowadzonego nazwiska
	if (pomocniczy == NULL) {
		return false;
	}// Nie znaleziono takiego nazwiska
	else {
		if (pomocniczy == (*glowa)) {
			(*glowa) = (*glowa)->nastepny;
			delete pomocniczy;
		}//usuwanie szukanego nazwiska
		else {
			pomocniczy1->nastepny = pomocniczy->nastepny;
			delete pomocniczy;
		}
	}
	return true;
}
bool usun_elementy(Element **glowa) {
	if ((*glowa) == NULL) {
		cout << "Lista Pusta" << endl;
		return false;
	}// Lista jest pusta
	Element *pomocniczy = (*glowa);
	Element *pomocniczy1 = NULL;
	while (pomocniczy != NULL) {
		pomocniczy1 = pomocniczy;
		pomocniczy = pomocniczy->nastepny; // Przejscie wskaznika na nastepny element 
		delete pomocniczy1; // Usuniecie elementu listy
	}
	(*glowa) = NULL; // Ustawienie elementu zerowego na NULL
	return true;
}
int main()
{	
	string  a;
	int opt=0;
	Element *glowa = NULL; // wskaznik na PIERWSZY element
    
	do {
		cout <<"%%%%% MENU PROGRAMU %%%%%" <<endl;
		cout << "1. Dodaj element do listy" << endl;
		cout << "2. Usuwanie pojedynczego elementu z listy" << endl;
		cout << "3. Wyswietlenie zawartosci listy" << endl;
		cout << "4. Usuniecie calej zawartosci listy" << endl;
		cout << "5. Wyjscie z programu" << endl;
		cout << "Podaj numer opcji menu, ktora Cie interesuje: ";
		cin >> opt;
		system("cls");
		switch (opt) {
		case 1: dodaj_element(&glowa); cout << endl << "Dodano element do listy"<<endl; Sleep(2000); break;
		case 2: cout << "Podaj nazwisko do usuniecia: "; cin >> a; usun_element(&glowa, a); cout << endl << "Usunieto element" << endl; Sleep(2000); break;
		case 3: if (glowa == NULL) cout << endl << "Lista pusta" << endl; else wypisz_elementy(glowa); Sleep(5000); break;
		case 4: usun_elementy(&glowa); cout << endl << "Usunieto wszystkie elementy listy" << endl; Sleep(2000);  break;
		case 5: break;
		}
		system("cls");
	} while (opt != 5);
	return 0;
}

