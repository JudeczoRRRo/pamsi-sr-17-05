// ConsoleApplication7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <ctime>
using namespace std;
int ilosc = 0;
string palList[10000];

bool jestPal(const string& testStr, int poczatek, int koniec) {
	if (testStr[poczatek] == testStr[koniec]) {
		if (koniec - poczatek <1) {
			return true;
		}
		else {
			jestPal(testStr, poczatek + 1, koniec - 1);
		}
	}
	else return false;
}
void permutacje(string& testStr, int licznik) {
	if (licznik < testStr.length())
		for (int i = licznik; i < testStr.length(); i++) {
			swap(testStr[licznik], testStr[i]);		
			permutacje(testStr, licznik + 1);	
			swap(testStr[licznik], testStr[i]);		
		}
	else {
		//cout << endl << testStr; 				
		if (jestPal(testStr, 0, testStr.length() - 1)) {
			//cout << "          <--- TO JEST PERMUTACJA";
			palList[ilosc++] = testStr;
		}
	}
}
void wyswietlListe() {
	cout << endl << endl << endl << "***** Palidromy *****" << endl << endl;
	for (int i = 0; i < ilosc; i++) {
		cout << palList[i] << endl;
	}
}
void czyscListe() {
	int j = 0;
	for (int i = 0; i < ilosc; i++) {
		for (int j = 0; j < ilosc; j++)
		{
			if (i != j)
				if (palList[i] != "")
					if (palList[i] == palList[j])  palList[j] = "";
		}
	}
	while (j < ilosc) {
		if (palList[j] == "") {
			for (int k = j; k < ilosc; k++) {
				palList[k] = palList[k + 1];
			}
			ilosc = ilosc - 1;
		}
		else j++;
	}  
	for (int i = 0; i < ilosc; i++) {
		cout << palList[i] << endl;
	}
}
int main()
{
	string testStr;
	cout << "Podaj slowo: ";
	cin >> testStr;
	clock_t start = clock();
	permutacje(testStr, 0);
	//wyswietlListe();
	cout << endl << endl << endl << "***** OCZYSZCZONA LISTA PERMUTACJI *****" << endl << endl;
	czyscListe();
	cout << "Liczba Palidromow: " << ilosc;
	printf("Czas wykonywania: %lu ms\n", (clock() - start));
	system("pause");
	return 0;
}
