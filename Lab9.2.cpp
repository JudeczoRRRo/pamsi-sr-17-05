// Lab9.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "graf_macierz_sasiedztwa.h"
#include <iostream>
#include <time.h>
#include <cstdlib>
#include <windows.h>
#include <fstream>
#include <exception>

using namespace std;

//////////////////		DFS		////////////////////

// Funkcja ustawia wszystkie etykiety element�w grafu na false oraz wywoluje druga czesc algorytmu, IN: graf

template <typename Typ>
void DFS(Graph<Typ> &G)
{
	int i;
	for (i = 0; i<G.sizeVert(); i++)
		G.vertices(i)->setLabel(false);
	for (i = 0; i<G.sizeEdge(); i++)
		G.edges(i)->setLabel(false);
	for (int i = 0; i<G.sizeVert(); i++)
	{
		if (G.vertices(i)->getLabel() == false)
			DFS(G, G.vertices(i));
	}
}

//	Funkcja wywoluje przejscie grafu w glab, IN: graf, wierzcholek;

template <typename Typ>

void DFS(Graph<Typ> &G, Vertex<Typ>* v)
{
	v->setLabel(true);
	for (int i = 0; i < G.sizeIncident(v); i++)
	{
		Edge<Typ>* e = G.incidentEdges(v, i);
		if (e->getLabel() == false)
		{
			Vertex<Typ>* w = G.opposite(v, e);
			if (w->getLabel() == false)
			{
				e->setLabel(true);
				DFS(G, w);
			}
			else
				e->setLabel(-1);
		}
	}
}

//////////////////		BFS		//////////////////

// Funkcja ustawia wszystkie etykiety grafu na false oraz wywoluje druga czesc algorytmu, IN: graf

template <typename Typ>

void BFS(Graph<Typ> &G)
{
	int i;
	for (i = 0; i<G.sizeVert(); i++)
		G.vertices(i)->setLabel(false);
	for (i = 0; i<G.sizeEdge(); i++)
		G.edges(i)->setLabel(false);
	for (i = 0; i<G.sizeVert(); i++)
	{
		if (G.vertices(i)->getLabel() == false)
			BFS(G, G.vertices(i));
	}
}

//	Funkcja wykonuje przeszukanie grafu wszerz, IN: graf, wierzcholek

template <typename Typ>
void BFS(Graph<Typ> &G, Vertex<Typ>* s)
{
	deque<Vertex<Typ>*> L;
	L.push_back(s);
	s->setLabel(true);

	while (!L.empty())
	{
		Vertex<Typ>* v = L.front();
		L.pop_front();
		for (int k = 0; k<G.sizeIncident(v); k++)
		{
			Edge<Typ>* e = G.incidentEdges(v, k);
			if (e->getLabel() == false)
			{
				Vertex<Typ>* w = G.opposite(v, e);
				if (w->getLabel() == false)
				{
					e->setLabel(true);
					w->setLabel(true);
					L.push_back(w);
				}
				else e->setLabel(-1);
			}
		}
	}
}

//	Funkcja losuje tablice losowych elementow o podanej ilosci i podanym przedziale,
//IN: tab[] - tablica losowych elementow
//	  ile - ilosc elementow
//    przedzial - przedzial w jakim wylosujemy liczby od 0 do ...

void wylosuj(int tab[], int ile, int przedzial)
{
	srand(time(NULL));

	int wylosowanych = 0;
	do
	{
		int liczba = (rand() % przedzial) + 1;
		tab[wylosowanych] = liczba;
		wylosowanych++;
	} while (wylosowanych <= ile);
}

//	Funkcja generuje graf i wypelnia go losowymi elementami, IN: graf, ilosc wierzcholkow i gestosc grafu

void generowanie_grafu(Graph<int> &G, int ilosc_wierzcholkow, float gestosc_grafu)
{
	int ilosc_krawedzi = ilosc_wierzcholkow*(ilosc_wierzcholkow - 1)*gestosc_grafu*0.5; //zmienna przechowuje ilosc krawedzi
	int *tab_wierz = new int[ilosc_wierzcholkow];
	int *tab_kraw = new int[ilosc_krawedzi];
	wylosuj(tab_wierz, ilosc_wierzcholkow, ilosc_wierzcholkow);
	wylosuj(tab_kraw, ilosc_krawedzi, ilosc_krawedzi);
	//dodawanie wierzcholkow
	for (int i = 0; i<ilosc_wierzcholkow; i++)
		G.insertVert(tab_wierz[i]);

	int k = 0;
	int i = 0;
	//petla laczy wierzcholki krawedziami w celu uzyskania zadanej gestosci grafu
	while (k < ilosc_krawedzi)
	{
		for (int j = i + 1; j<ilosc_wierzcholkow; j++)
		{
			if (k < ilosc_krawedzi)
			{
				G.insertEdge(tab_wierz[i], tab_wierz[j], tab_kraw[k]);
				k++;
			}
			else break;
		}
		i++;
	}
	
}



int main()
{	
	fstream plik1, plik2;
	clock_t start, stop, czas;
	plik1.open("pomiaryDFS 100.25.txt", ios::out);
	plik2.open("pomiaryBFS 100.25.txt", ios::out);
	Graph <int> graf;
	int ilosc_wierzcholkow = 100;
	float gestosc = 0.25;
	for (int i = 0; i < 50; i++) {
		//cout << "Skonczono generowac graf" << endl;
		start = clock();
		DFS(graf);
		stop = clock();
		czas = stop - start;
		//cout << "Czas dla " << i << " grafu DFS to " << czas << " ms" << endl;
		plik1 << czas << endl;
		start = clock();
		BFS(graf);
		stop = clock();
		czas = stop - start;
		//cout << "Czas dla " << i << "grafu BFS to " << czas << " ms" << endl;
		plik2 << czas << endl;
		system("cls");
		cout << (i + 1) * 2 << "%" << endl;
	}
	plik1.close();
	plik2.close();
	system("pause");
	return 0;
}

