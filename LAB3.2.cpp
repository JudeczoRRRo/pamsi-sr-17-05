// LAB3.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <ctime>
#include "queue.h"
#include "queueSTL.h"

using namespace std;

int main()
{
	int opt_m1 = 0, opt_m2 = 0, opt_m3 = 0, element, element_stl, ilosc, petla;
	Queue <int> queue(1000);
	std::queue < int > queue_stl;
	do {
		cout << endl << "***MENU***" << endl << endl;
		cout << "1. Kolejka na bazie tablicy" << endl;
		cout << "2. Kolejka na kontenerach STL" << endl;
		cout << "3. Pomiar czasow dzialania obu kolejek" << endl;
		cout << "Podaj numer opcji ktora cie interesuje: ";
		cin >> opt_m1;
		cout << endl;
		switch (opt_m1) {
		case 1:
			do {
				cout << endl << "***MENU KOLEJKI NA BAZIE TABLICY***" << endl << endl;
				cout << "1. Dodaj element do kolejki" << endl;
				cout << "2. Usun element z kolejki" << endl;
				cout << "3. Usun wszystkie elementy z kolejki" << endl;
				cout << "4. Wyswietl kolejke" << endl;
				cout << "5. Powrot do menu glownego" << endl;
				cout << "Podaj numer opcji ktora cie interesuje: ";
				cin >> opt_m2;
				cout << endl;
				switch (opt_m2) {
				case 1:
					cout << "Podaj liczbe dodamy ja do kolejki: ";
					cin >> element;
					queue.enqueue(element);
					cout << endl << "Element zostal dodany" << endl;
					break;
				case 2:
					queue.dequeue();
					cout <<endl << "Element zostal usuniety" << endl;
					break;
				case 3:
					queue.dequeue_all();
					cout << endl << "Wszystkie elementy zostaly usuniete" << endl;
					break;
				case 4:
					queue.show();
					break;
				case 6:
					cout << "Podaj ile losowych liczb chcesz dodac do kolejki: " << endl;
					cin >> ilosc;
					for (int i = 0; i < ilosc; i++) {
						element = rand() % 10 + 0;
						queue.enqueue(element);
					}
					break;
				}
			} while (opt_m2 != 5);
			break;
		case 2: do {
			cout << endl << "***MENU KOLEJKI STL***" << endl << endl;
			cout << "1. Dodaj element do kolejki" << endl;
			cout << "2. Usun element z kolejki" << endl;
			cout << "3. Usun wszystkie elementy z kolejki" << endl;
			cout << "4. Wyswietl kolejke" << endl;
			cout << "5. Powrot do menu glownego" << endl;
			cout << "Podaj numer opcji ktora cie interesuje: ";
			cin >> opt_m3;
			cout << endl;
			switch (opt_m3) {
			case 1: 
				cout << "Podaj liczbe dodamy ja do kolejki: ";
				cin >> element_stl;
				queue_stl.push(element_stl); 
				cout << "Element zostal dodany" << endl;
				break;
			case 2: 
				if (queue_stl.empty()) {
					cout << "Kolejka jest pusta" << endl;
				}
				else {
					queue_stl.pop();
					cout << endl << "Element zostal usuniety" << endl;
				}
				break;
			case 3: 
				petla = queue_stl.size();
				for (int i = 0; i < petla; i++) {
					queue_stl.pop();
				}
				if(petla!=0) cout << endl << "Wszystkie elementy zostaly usuniete" << endl;
				break;
			case 4: break;
			case 6: 
				cout << "Podaj ile losowych liczb chcesz dodac do kolejki: " << endl;
				cin >> ilosc;
				for (int i = 0; i < ilosc; i++) {
					element_stl = rand() % 10 + 0;
					queue_stl.push(element_stl);
				}
				break;
			}
		} while (opt_m3 != 5); 
		break;
		case 3: 
			clock_t start, stop;
			cout << "Podaj ile losowych liczb chcesz dodac do kolejki: " << endl;
			cin >> ilosc;
		
			start = clock();
			for (int i = 0; i < ilosc; i++) {
				element_stl = rand() % 10 + 0;
				queue_stl.push(element_stl);
			}
			stop = clock();
			printf("Czas wykonywania dla kolejki STL: %lu ms ~ %lu s\n", (stop - start), (stop - start) / 1000);
			start = clock();
			for (int i = 0; i < ilosc; i++) {
				element = rand() % 10 + 0;
				queue.enqueue(element);
			}
			stop = clock();
			printf("Czas wykonywania dla kolejki na bazie tablicy: %lu ms ~ %lu s\n", (stop - start), (stop - start) / 1000);
			break;
		}
	
	} while (opt_m1 != 4);
	system("pause");
    return 0;
}

