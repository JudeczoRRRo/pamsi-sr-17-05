
template <typename Object>

class TabStack {
private:
	int pojemnosc;
	Object *stos;
	int glowa;
public:
	TabStack(int p) {
		pojemnosc = p;
		stos = new Object[pojemnosc];
		glowa = -1;
	}
	bool isEmpty_t();
	Object pop_t();
	void push_t(int element);
	void show_t();
	Object pop_all_t();
};

template <typename Object>
bool TabStack<Object>::isEmpty_t()
{
	return (glowa < 0);
}

template<typename Object>
inline Object TabStack<Object>::pop_t()
{
	if (isEmpty_t())
		cout << "Stos jest pusty";
	else return stos[glowa--];
}
template<typename Object>
inline void TabStack<Object>::push_t(int element)
{
	if (pojemnosc - 1 == glowa) {
		Object* temp = new Object[pojemnosc * 2];
		for (int i = 0; i < pojemnosc - 1; i++) {
			temp[i] = stos[i];
		}
		stos = temp;
		pojemnosc = pojemnosc * 2;
	}
	glowa += 1;
	stos[glowa] = element;
}
template<typename Object>
inline void TabStack<Object>::show_t()
{
	for (int i=0; i < glowa+1; i++) {
		cout << stos[i] << " ";
	}
}

template<typename Object>
inline Object TabStack<Object>::pop_all_t()
{	
	for (int i = 0; i < glowa + 1; i++) {
		pop_t();
	}

	return glowa = -1;
}

