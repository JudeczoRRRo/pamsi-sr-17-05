// ConsoleApplication4.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>

using namespace std;


int main()
{
	int n, m, x, max = 0;
	cout << "Podaj liczbe wierszy: ";
	cin >> n;
	cout << "Podaj liczbe kolumn: ";
	cin >> m;
	cout << "Podaj maksymalna losowa liczbe: ";
	cin >> x;

	double **tab = new double *[n]; //alokacja pamieci
	for (int i = 0; i < n; ++i)
	{
		tab[i] = new double[m]; //alokacja pamieci
		for (int j = 0; j < m; ++j) //wpisanie wartosci do tablicy
			tab[i][j] = rand()%(x+1) + 0;
	}
	
	for (int i = 0; i < n; ++i, cout << endl)
		for (int j = 0; j < m; ++j)
			cout << tab[i][j] << '\t';
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < m; ++j) {
			if (tab[i][j] > max)
				max = tab[i][j];
		}
	cout << "Maksymalna liczba w tabeli: " << max << endl;
	for (int i(0); i < n; ++i)
		delete[] tab[i]; //uwolnienie pamieci
	delete[] tab; //uwolnienie pamieci
	tab = NULL;

	system("PAUSE");
	return 0;
}


