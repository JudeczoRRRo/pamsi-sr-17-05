// Laborki 3.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stack>
#include <ctime>
#include <iostream>
#include "TabStack.h"

using namespace std;

int main()
{
	int opt_m1 = 0, opt_m2 = 0, opt_m3 = 0, element, element_stl, ilosc,petla;
	TabStack <int> stos(1000);
	std::stack<int>stos_stl;
	do {
		cout << endl << "***MENU***" << endl << endl;
		cout << "1. Stos na bazie tablicy" << endl;
		cout << "2. Stos na kontenerach STL" << endl;
		cout << "3. Pomiar czasow dzialania obu Stosow" << endl;
		cout << "Podaj numer opcji ktora cie interesuje: ";
		cin >> opt_m1;
		switch (opt_m1) {
		case 1:
			do {
				cout << endl << "***MENU STOSU NA BAZIE TABLICY***" << endl << endl;
				cout << "1. Dodaj element do stosu" << endl;
				cout << "2. Usun element ze stosu" << endl;
				cout << "3. Usun wszystkie elementy ze stosu" << endl;
				cout << "4. Wyswietl stos" << endl;
				cout << "5. Powrot do menu glownego" << endl;
				cout << "Podaj numer opcji ktora cie interesuje: ";
				cin >> opt_m2;
				switch (opt_m2) {
				case 1:
					cout << "Podaj liczbe dodamy ja do stosu: ";
					cin >> element;
					stos.push_t(element);
					cout << endl << "Element zostal dodany" << endl;
					break;
				case 2:
					stos.pop_t();
					cout << endl << "Element zostal usuniety" << endl;
					break;
				case 3:
					stos.pop_all_t();
					cout << endl << "Wszystkie elementy zostaly usuniete" << endl;
					break;
				case 4:
					stos.show_t();
					break;
				case 6:
					cout << "Podaj ile losowych liczb chcesz dodac do stosu: ";
					cin >> ilosc;
					for (int i = 0; i < ilosc; i++) {
						element = rand() % 10 + 0;
						stos.push_t(element);
					}
					break;
				}
			} while (opt_m2 != 5);
			break;
		case 2:
			do {
				cout << endl << "***MENU STOSU NA BAZIE TABLICY***" << endl << endl;
				cout << "1. Dodaj element do stosu" << endl;
				cout << "2. Usun element ze stosu" << endl;
				cout << "3. Usun wszystkie elementy ze stosu" << endl;
				cout << "4. Wyswietl stos" << endl;
				cout << "5. Powrot do menu glownego" << endl;
				cout << "Podaj numer opcji ktora cie interesuje: ";
				cin >> opt_m3;
				switch (opt_m3) {
				case 1:
					cout << "Podaj liczbe dodamy ja do stosu: ";
					cin >> element_stl;
					stos_stl.push(element_stl);
					cout << endl << "Element zostal dodany" << endl;
					break;
				case 2:
					if (stos_stl.empty()) {
						cout <<endl<< "Stos jest pusty" << endl;
					}
					else stos_stl.pop();
					cout << endl << "Element zostal usuniety" << endl;
					break;
				case 3:
					petla = stos_stl.size();
					for (int i = 0; i < petla; i++) {
						stos_stl.pop();
					}
					if(petla!=0) cout << endl << "Wszystkie elementy zostaly usuniete" << endl;
					break;
				case 4:
					break;
				case 6:
					cout << "Podaj ile losowych liczb chcesz dodac do stosu: ";
					cin >> ilosc;
					for (int i = 0; i < ilosc; i++) {
						element_stl = rand() % 10 + 0;
						stos_stl.push(element_stl);
					}
					break;
				}
			} while (opt_m3 != 5);
			break;
		case 3: 
			clock_t start, stop;
			cout << "Podaj ile losowych liczb chcesz dodac do stosu: " << endl;
			cin >> ilosc;
			start = clock();
			for (int i = 0; i < ilosc; i++) {
				element_stl = rand() % 10 + 0;
				stos_stl.push(element_stl);
			}
			stop = clock();
			printf("Czas wykonywania dla stosu STL: %lu ms ~ %lu s\n", (stop - start), (stop - start) / 1000);
			start = clock();
			for (int i = 0; i < ilosc; i++) {
				element = rand() % 10 + 0;
				stos.push_t(element);
			}
			stop = clock();
			printf("Czas wykonywania dla stosu na bazie tablicy: %lu ms ~ %lu s\n", (stop - start), (stop - start) / 1000);
			break;
		}
	} while (opt_m1 != 4);
	system("pause");
    return 0;
}

